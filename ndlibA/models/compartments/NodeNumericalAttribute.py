from ndlibA.models.compartments.Compartment import Compartiment
import numpy as np
import operator

__author__ = 'Giulio Rossetti'
__license__ = "BSD-2-Clause"
__email__ = "giulio.rossetti@gmail.com"



## NEW ARRAY BASED COMPARTMENT
class NodeNumericalAttribute(Compartiment):
    
    def __init__(self, attribute, value=None, op=None, rate=1, triggering_status=None, **kwargs):
        super(self.__class__, self).__init__(kwargs)
        self.__available_operators = {"==": operator.__eq__, "<": operator.__lt__,
                                      ">": operator.__gt__, "<=": operator.__le__,
                                      ">=": operator.__ge__, "!=": operator.__ne__,
                                      "IN": (operator.__ge__, operator.__le__)}

        self.attribute = attribute
        self.attribute_range = value
        self.operator = op
        self.rate = rate
        self.trigger = triggering_status

        if self.attribute_range is None:
            raise ValueError("A valid attribute value must be provided")

        if self.operator is not None and self.operator in self.__available_operators:
            if self.operator == "IN":
                if not isinstance(self.attribute_range, list) or self.attribute_range[1] < self.attribute_range[0]:
                    raise ValueError("A range list is required to test IN condition")
            else:
                if not isinstance(self.attribute_range, int):
                    if not isinstance(self.attribute_range, float):
                        raise ValueError("A numeric value is required to test the selected condition")
        else:
            raise ValueError("The operator provided '%s' is not valid" % operator)

    def execute(self, adjacency, edges, attributes, status_map, to_use, *args, **kwargs):
        '''
        adjacency is a non-symmetric adjacency matrix: if the edge i,j exists, in i,j there is the status of j, in j,i the status of i. otherwise, in i,j there is 0
        nb: statuses now start from 1
        nb: we assume that the graph is undirected
        '''
        
        if self.trigger is None:
            triggered = to_use 
        else:
            triggered = (adjacency==status_map[self.trigger]).sum(axis=1)
            triggered = np.asarray(triggered, dtype=bool).reshape(to_use.shape)*to_use
        
        vals = attributes.get(self.attribute)
        if vals is None:
            raise ValueError("The specified attribute {} is not among the available attributes".format(self.attribute))

        if self.operator == "IN":
            triggered = triggered*self.__available_operators[self.operator][0](vals, self.attribute_range[0])*self.__available_operators[self.operator][1](vals, self.attribute_range[1])
        else:
            triggered = triggered*self.__available_operators[self.operator](vals, self.attribute_range)
        
        p = np.random.random(triggered.sum())
        test = np.zeros(triggered.shape, dtype=bool)
        try:
            test[triggered] = (p < self.rate)
        except:
            test[triggered] = (p < self.rate[triggered])
        if np.any(test):
            return np.logical_and(test, self.compose(adjacency, edges, attributes, status_map, to_use, kwargs))
        else:
            return test
