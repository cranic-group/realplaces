from ndlibA.models.compartments.Compartment import Compartiment
import numpy as np

__author__ = 'Giulio Rossetti'
__license__ = "BSD-2-Clause"
__email__ = "giulio.rossetti@gmail.com"



## NEW ARRAY BASED COMPARTMENT
class CountDown(Compartiment):

    def __init__(self, name, iterations, **kwargs):
        super(self.__class__, self).__init__(kwargs)
        try:
            self.iterations = int(iterations)
        except:
            # if iterations is not a number, try to execute it -- for instance, this allows passing a random generator for the number of iterations 
            self.iterations = iterations()
        self.name = name

    def execute(self, adjacency, edges, attributes, status_map, to_use, *args, **kwargs):
        if self.name in attributes:
            not_zeros = attributes[self.name]>0
            attributes[self.name] -= 1*to_use*(not_zeros)
        else:
            attributes[self.name] = np.ones(to_use.shape, dtype=int)*self.iterations
            not_zeros = np.ones(to_use.shape, dtype=bool)

        test = (attributes[self.name]==0)*not_zeros
        if np.any(test):
            return np.logical_and(test, self.compose(adjacency, edges, attributes, status_map, to_use, kwargs))
        else:
            return test
