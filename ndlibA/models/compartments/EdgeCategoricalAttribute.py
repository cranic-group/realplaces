from ndlibA.models.compartments.Compartment import Compartiment
import numpy as np
from scipy.sparse import csr_matrix

__author__ = 'Giulio Rossetti'
__license__ = "BSD-2-Clause"
__email__ = "giulio.rossetti@gmail.com"



## NEW ARRAY BASED COMPARTMENT
class EdgeCategoricalAttribute(Compartiment):

    def __init__(self, attribute, value, triggering_status=None, probability=1, **kwargs):
        super(self.__class__, self).__init__(kwargs)
        self.attribute = attribute

        if not isinstance(value, str):
            raise ValueError("Categorical (string) value expected")

        self.attribute_value = value
        # trigger is now allowed to be a list of statuses
        if isinstance(triggering_status,str):
            triggering_status = [triggering_status]
        self.trigger = triggering_status
        self.probability = probability

    def execute(self, adjacency, edges, attributes, status_map, to_use, *args, **kwargs):
        '''
        adjacency is a non-symmetric adjacency matrix: if the edge i,j exists, in i,j there is the status of j, in j,i the status of i. otherwise, in i,j there is 0
        nb: statuses now start from 1
        nb: we assume that the graph is undirected
        '''

        if self.trigger is None:
            triggered = edges[self.attribute].get(self.attribute_value,csr_matrix(adjacency.shape))
        else:
            triggered = sum(adjacency==status_map[t] for t in self.trigger).multiply(edges[self.attribute].get(self.attribute_value,False))
        triggered = triggered[to_use]
        nz = triggered.count_nonzero() 
        if nz:
            p = csr_matrix((np.random.random(nz),triggered.nonzero()), shape=triggered.shape)
            t = (p>=1-self.probability).sum(axis=1)
            t = np.asarray(t, dtype=bool).reshape(to_use.sum())
            test = np.zeros(to_use.shape, dtype=bool)
            test[to_use] = t
        else:
            test = np.zeros(to_use.shape, dtype=bool)

        if np.any(test):
            return np.logical_and(test, self.compose(adjacency, edges, attributes, status_map, to_use, kwargs))
        else:
            return test
