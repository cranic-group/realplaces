import argparse
import numpy as np

parser=argparse.ArgumentParser(description="Create random groups for testing ppr_real")
parser.add_argument('-g', '--num_groups', dest='num_groups', type=int, default=1000, help='number of groups entering the park')
parser.add_argument('-p', '--num_people', dest='num_people', type=int, default=100000, help='numer of people in the population')
args=parser.parse_args()

N = args.num_people
G = args.num_groups
idxs = np.random.permutation(range(N)).tolist()
print('group_size, member0, age0, member1, age1, member2, age2')
for i in range(G):
    size = 1+np.random.choice(3)
    s = str(size)
    for j in range(3):
        if j<size:
            s += ', {}, {}'.format(idxs.pop(),np.random.choice(4))
        else:
            s += ', , '
    print(s)
