#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import os
import sys
from sys import getsizeof
from distutils.util import strtobool
import pickle
from   numpy.random import default_rng
import numpy as np
import subprocess
import pandas as pd
import configparser as cp
from datetime import datetime
from scipy.sparse import csr_matrix, coo_matrix, vstack, hstack
from scipy.stats import lognorm, pareto
from scipy.spatial.distance import cdist
from timeit import default_timer as timer
from collections import Counter
from itertools import combinations
import igraph as ig
#####
from lib.libfunc import read_from_config, project_coord, build_projected_territory, build_grid_dictionary


###############################################################################
#
#                       READ CONFIG, BUILD DATA STRUCT
#
###############################################################################


def read_pop_info(in_path, config_file, city=None):
    """
    Read information about the population structure from the config file. 
    The poopulation is defined by a set of parameters:
        - 'city' is the city of interest 
        - 'size' is the number of individuals in the population
        - 'group_names' is the list of age-group names (e.g., ['children', 'young', 'adults', 'elderly'])
        - 'group_freqs' is the list of age-group frequencies (i.e., frequency of each age-group in the population); the order is consistent with 'group_names' and the sum must be 1
        - 'households' is a boolean that establishes whether households must be included in the simulation
        - 'fitness_func' is a string that determines which distribution is used to determine the fitness score associated to each individual
    
    :param str in_path: path to the root folder of all input data
    :param str config_file: full path to the config file
    :param city: the city of interest; if None, the city is read from config
    :type city: str or None
    :return: the pop_info dictionary containing the aforementioned information 
    """
    
    config = cp.ConfigParser()
    config.read(config_file)
    if not city:
        city    = eval(config.get('CITY', 'city'))
    N       = eval(config.get('POPULATION', 'size'))
    # list of age-group names 
    group_names = eval(config.get('POPULATION', 'group_names'))
    groups  = eval(config.get('POPULATION', 'groups'))
    if groups=='uniform':
        groups = {group_name:1/len(group_names) for group_name in group_names}
    # groups are computed from the population file if groups=='real'
    if groups=='real' or N==0:
        pop_filename = os.path.join(in_path,eval(config.get('POPULATION', 'popfile'))[city])
        pop = pd.read_csv(pop_filename)
        if groups=='real':
            groups = {group_names[k]:v/len(pop) for k,v in sorted(Counter(pop.type).items())}
        if N==0:
            N = len(pop)

    fitness_func = eval(config.get('POPULATION', 'fitness_func'))
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Fitness function: {fitness_func}")
    households   = config.getboolean('POPULATION', 'households')
    percSum = 0
    for k,v in groups.items():
        percSum += v
    if percSum < 0.999999999 or percSum > 1.00000000001:
        print( "The percentages of the groups must sum to 1, now it's {}".format(percSum) , file=sys.stderr)
        sys.exit(1)
        
    # list of frequency for each group
    group_freqs = list(groups.values())

    pop_info = {'city':city, 'size':N, 'group_names': group_names, 'group_freqs': group_freqs, 'households': households, 'fitness_func': fitness_func}
    
    return pop_info


# HIGH LEVEL WRAPPER FUNCTION TO MAKE THE CODE MORE READABLE 
def get_input_data_structures(in_path, config_file, city=None, verbose=False):
    """
    Read config file and build the data structures that store information on the territory, the population and the distances.
    
    :param str in_path: path to the root folder of all input data
    :param str config_file: full path to the config file
    :param city: the city of interest; if None, the city is read from config
    :type city: str or None
    :param bool verbose: whether to print information about the options read from config
    :return: a nested dictionary containing the three dictionaries: territory, pop_info and dist_info
    """

    # Read the territory (origin, bbox, tiles, ...)
    inCRS  = read_from_config(config_file, 'PROJECTION', 'input_CRS')
    outCRS = read_from_config(config_file, 'PROJECTION', 'output_CRS')
    territory = build_projected_territory(config_file, inCRS, outCRS, city=city)

    # Read the pop_info (population size, age-groups, density, ...)
    pop_info = read_pop_info(in_path, config_file, city=city)

    # Read the dist_info (the distance function D and the distance between nodes in the same tile)
    zero_dist = read_from_config(config_file, 'DISTANCES', 'zero_dist')
    dist_func = read_from_config(config_file, 'DISTANCES', 'dist_func')
    if isinstance(dist_func, tuple):
        dist_func, dist_power = dist_func
    else:
        dist_power = None
    if dist_func == 'one':
        zero_dist = 1
    dist_info = {'zero_dist':zero_dist, 'dist_func':dist_func, 'dist_power':dist_power}
    
    if verbose:
        print("Chosen input distance function: {}".format(dist_info))

    return {'territory':territory, 'pop_info':pop_info, 'dist_info':dist_info}



###############################################################################
#
#                           POPULATION
#
###############################################################################

def gen_random_coordinates(territory, pop_size=10, gentype='uniform', tiles_to_use=None, get_tile_id=False, rng=None): 
    """ 
    Generate the coordinates (x, y) of pop_size points inside the territory.
    Two identical coordinates are considered admissible.
    If the parameter 'get_tile_id' is True, also the id of each node's tile is returned.

    :param dict territory: a dict as returned by build_projected_territory
    :param int pop_size: the number of X,Y coordinates to generate (num of individuals)
    :param str gentype: how to generate the coordinates; can be "uniform" or "normal" 
    :param tiles_to_use: whether to only generate coordinates in selected tiles
    :type tiles_to_use: sequence of integers or None 
    :param bool get_tile_id: whether to return the tile ids
    :param rng: random number generator to be used
    :type rng: Generator or int or None
    :return: two numpy arrays xarray, yarray of length pop_size which contain the x and y coordinates of all nodes; if get_tile_id is True, a third array xytile is return that contains the tile id of all nodes            
    """
        
    tx, ty   = territory['tile']
    ntx, nty = territory['ntiles']
    x0, y0   = territory['origin']
    
    (max_x, min_x, max_y, min_y) = territory['borders']

    rng = default_rng(rng)
    
    if tiles_to_use is not None:
        if gentype == 'uniform':   
            probs = np.ones(len(tiles_to_use))/len(tiles_to_use)
        elif gentype == 'normal':
            xdim = tx*ntx
            ydim = ty*nty
            clat = y0 + ydim/2.0
            clon = x0 + xdim/2.0
            sigma_y = ydim/10.0
            sigma_x = xdim/10.0
            yarray = rng.normal(clat, sigma_y, 10000000)
            xarray = rng.normal(clon, sigma_x, 10000000)
            xtile  = (xarray - x0)//tx
            ytile  = (yarray - y0)//ty
            xytile = xtile + ntx*ytile
            counter = dict(zip(*np.unique(xytile, return_counts=True)))
            probs = np.asarray([counter.get(tid,0) for tid in tiles_to_use])
            probs = probs/probs.sum()
        else:
            print("ERROR, unknown random generator type: {}".format(gentype))
        xytile = rng.choice(tiles_to_use, p=probs, size=pop_size)
        xtile  = xytile % ntx
        ytile  = xytile // ntx
        xarray = rng.uniform(tx, pop_size)+xtile*tx
        yarray = rng.uniform(ty, pop_size)+ytile*ty
    else:
        if gentype == 'uniform':   
            xarray = rng.uniform(min_x, max_x, pop_size)
            yarray = rng.uniform(min_y, max_y, pop_size)
            xtile  = (xarray - x0)//tx
            ytile  = (yarray - y0)//ty
            xytile = xtile + ntx*ytile
        elif gentype == 'normal':
            xdim = tx*ntx
            ydim = ty*nty
            clat = y0 + ydim/2.0
            clon = x0 + xdim/2.0
            sigma_y = ydim/10.0
            sigma_x = xdim/10.0
            yarray = rng.normal(clat, sigma_y, pop_size)
            xarray = rng.normal(clon, sigma_x, pop_size)
            xtile  = (xarray - x0)//tx
            ytile  = (yarray - y0)//ty
            xytile = xtile + ntx*ytile
        else:
            print("ERROR, unknown random generator type: {}".format(gentype))
        
    xytile = xytile.astype(int)
    
    if get_tile_id is False:
        return xarray, yarray
    else:
        return xarray, yarray, xytile
    

def get_pop_types(group_freqs, N, gentype='uniform', rng=None):
    """
    Generate the type (i.e., age-group) of each of the N individuals of the population.
    The type is an integer i in [0,...,k-1] where k is the number of available age-groups, equal to the length of group_freqs.

    :param list group_freqs: the frequency of each age-group, this list must sum to 1
    :param int N: the number of individuals in the population
    :param str gentype: how to generate the types; currently, only 'uniform' is supported
    :param rng: random number generator to be used
    :type rng: Generator or int or None
    :return: a numpy array of N integers representing the types of individuals in the population
    """
    # prefix sum of group freqs
    pgsum = np.cumsum(group_freqs)
    
    # Init the generator
    rng = default_rng(rng)
    if gentype == 'uniform':
        # Extract N random number f in (0,1)
        f = rng.uniform(0, 1, N)
    else:
        print("Error: gen_pop_type: generator {} not supported! Abort.", file=sys.stderr)
        exit(111)
    
    # Find the indices of the element where f lies. 
    # Indices corresponds to node types
    node_types = np.searchsorted(pgsum, f)
      
    return node_types


def get_fitness(fitness_func='one', N=1, rng=None):
    """
    Generate the fitness score for N individuals from the given probability distribution
        
    :param str fitness_func: how to generate the types; can be 'one' (constant f), 'uniform' (uniform f in (0,1)), 'lognormal', 'pareto' or 'truncated_pareto'; the parameters for lognormal and pareto distributions have been selected experimentally, you might want to adjust them; truncated_pareto is truncated at 100, you might want to change this as well
    :param int N: the number of individuals in the population
    :param rng: random number generator to be used
    :type rng: Generator or int or None
    :return: a numpy array of N floats representing the N requested fitness scores
    """

    rng = default_rng(rng)
    if fitness_func == 'one':
        return np.ones(N)
    elif fitness_func == 'lognormal':
        b=0.5
        loc=1
        scale=2
        r = lognorm.rvs(b, loc=loc, scale=scale, size=N)
        return r
    elif 'pareto' in fitness_func:
        b=3.7
        loc=-6
        scale=10
        r = pareto.rvs(b, loc=loc, scale=scale, size=N)
        if 'truncated' in fitness_func:
            while True:
                to_redraw = r>100
                num_to_redraw = to_redraw.sum()
                if num_to_redraw:
                    print("WARNING: {} draws repeated".format(num_to_redraw))
                    r_ = pareto.rvs(b, loc=loc, scale=scale, size=num_to_redraw)
                    r[to_redraw] = r_
                else:
                    break
        return r
    elif fitness_func == 'uniform':
        return rng.uniform(0,1,N)
    else:
        print("get_fitness: ERROR! unknown function '{}'".format(fitness_func))
        return 111

    
def gen_population(in_path, territory, pop_info, tiles_to_use=None, gentype_coord='uniform', rng=None, seed=0, verbose=False):
    """
    Build the population dictionary, calling get_pop_types, gen_random_coordinates and get_fitness.
    The dictionary contains the following numpy arrays or lists:
    - 'type': the type (i.e., age-group) of each individual
    - 'coord': the (x,y) coordinates of each individual
    - 'tile_id': the id of the tile to which the (x,y) coordinates belong, for each individual
    - 'fitness': the fitness score of each individual
    - 'household': the id of the household to which each individual belongs
    
    :param str in_path: path to the root folder of all input data
    :param dict territory: a dict as returned by build_projected_territory
    :param dict pop_info: a dict as returned by read_pop_info 
    :param tiles_to_use: whether to only generate coordinates in selected tiles
    :type tiles_to_use: sequence of integers or None 
    :param str gentype_coord: how to generate the coordinates; can be "uniform" or "normal" 
    :param rng: random number generator to be used for the population
    :type rng: Generator or int or None
    :param int seed: seed for the random number generator to be used for the fitness score
    :return: the population dictionary containing the aforementioned information 
    """

    N = pop_info['size']
   
    # ===> Generate a list of N types
    types = get_pop_types(pop_info['group_freqs'], N, rng=rng)

    # ===> Generate a list of N coordinates (x, y) and the corresponding tile in the grid
    x, y, tile_id = gen_random_coordinates(territory, pop_size=N, gentype=gentype_coord, tiles_to_use=tiles_to_use, get_tile_id=True, rng=rng)
   
    # ===> Generate node fitness
    fitness_rng = default_rng(seed)
    fitness = get_fitness(pop_info['fitness_func'], N, rng=fitness_rng)

    # sort by tile_id (needed by bf.c)
    order   = np.argsort(tile_id)
    types   = types[order]
    x       = x[order]
    y       = y[order]
    coord   = list(zip(x, y))
    tile_id = tile_id[order]
    fitness = fitness[order]
   
    if pop_info['households']:
        while True:
            temp_filename = 'population_gen_' + str(np.random.randint(100000)) + '.csv'
            tmp_file = os.path.join(in_path,'BuildSBG/'+temp_filename)
            if not os.path.exists(tmp_file):
                break
        with open(tmp_file, 'w') as f:
            print('tile_id,x,y,type', file=f)
            for tid,x_,y_,t in zip(tile_id,x,y,types):
                print('{},{},{},{}'.format(tid,x_,y_,t), file=f)
        # we pass temp_filename instead of tmp_file because compute_households looks for all files in in_path/BuildSBG
        hh = compute_households(in_path, pop_filename=temp_filename, seed=seed) 
        os.remove(tmp_file)
    else:
        hh = None
   
    if verbose:
        print("gen_population: generated {} nodes".format(N))
    
    return {'type': types, 'coord': coord, 'tile_id': tile_id, 'fitness': fitness, 'household':hh}


def read_population(in_path, pop_df, pop_info, territory, pop_filename='', read_type=True, rng=None, seed=0, verbose=False):
    """
    Build the population dictionary, using data-driven density and (possibly) age-groups.
    The dictionary contains the following numpy arrays or lists:
    Read the data-driven population creating a population dictionary that contains the following numpy arrays or lists:
    - 'type': the type (i.e., age-group) of each individual
    - 'coord': the (x,y) coordinates of each individual
    - 'tile_id': the id of the tile to which the (x,y) coordinates belong, for each individual
    - 'fitness': the fitness score of each individual
    - 'household': the id of the household to which each individual belongs
    
    :param str in_path: path to the root folder of all input data
    :param dataframe pop_df: a population dataframe that stores the location, age and possibly household for each individual 
    :param dict pop_info: a dict as returned by read_pop_info 
    :param dict territory: a dict as returned by build_projected_territory
    :param str pop_filename: the file that stores the population in csv format  
    :param bool read_type: whether the type (i.e., age-group) must be read from the dataframe or generated
    :param rng: random number generator to be used for the population
    :type rng: Generator or int or None
    :param int seed: seed for the random number generator to be used for the fitness score
    :param bool verbose: whether to print information about the population during the construction
    :return: the population dictionary containing the aforementioned information 
    """

    if not 'household' in pop_df.columns:
        if pop_info['households']:
            hh = compute_households(in_path, pop_filename=pop_filename, seed=seed)
            pop_df['household'] = hh
    else:
        hh = pop_df['household']
    
    sample_size = pop_info['size']
    if 0<sample_size<len(pop_df) and pop_info['households']:
        if verbose:
            print("read_population: sampling households from population to sum to {} people".format(sample_size))
        selected_hh = extract_households(hh, sample_size, rng=rng)
        pop_df = pop_df.iloc[np.concatenate(selected_hh)] 
    elif 0<sample_size<len(pop_df):
        pop_df = pop_df.sample(n=sample_size,random_state=seed)
 
    N = pop_info['size']
    if read_type:
        types = pop_df['type'].to_numpy(dtype=np.int)
    else:
        # ===> Generate a list of N types
        types = get_pop_types(pop_info['group_freqs'], N, rng=rng)

    fitness_rng = default_rng(seed)
    fitness = get_fitness(pop_info['fitness_func'], N, rng=fitness_rng)
    
    pop = {}
    if pop_info['households']:
        pop['household'] = pop_df['household'].to_numpy(dtype=np.int)
    else:
        pop['household'] = None
    pop['tile_id'] = pop_df['tile_id'].to_numpy(dtype=np.int)
    pop['coord'] = [(x, y) for x, y in zip(pop_df['x'].to_numpy(), pop_df['y'].to_numpy())]
    pop['type'] = types
    pop['fitness'] = fitness

    return pop


def get_population_dictionary(in_path, config_file, territory, pop_info, rng=None, verbose=False):
    """
    Build the population deciding whether it must be read or generated, and calling either read_population or gen_population.
    The choice is based on the 'mode' parameter, specified in the config file. 
    
    :param str in_path: path to the root folder of all input data
    :param str config_file: full path to the config file
    :param dict territory: a dict as returned by build_projected_territory
    :param dict pop_info: a dict as returned by read_pop_info 
    :param rng: random number generator to be used for the population
    :type rng: Generator or int or None
    :param bool verbose: whether to print information about the population during the construction
    :return: the population dictionary, as returned by read_population and gen_population 
    """

    # mode: reader or generator
    mode   = read_from_config(config_file, 'POPULATION', 'mode')
    if isinstance(mode, tuple):
        mode, gentype = mode 
    # check groups to see if age must be generated
    groups = read_from_config(config_file, 'POPULATION', 'groups')
   
    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Population mode: {mode}")
    seed = read_from_config(config_file, 'RANDOM', 'seed')
    if seed is None:
        seed=default_rng(seed).integers(10000)
        if verbose:
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - None seed in config file, extracted seed is {seed}")
    
    # get population df
    pop_file  = os.path.join(in_path, read_from_config(config_file, 'POPULATION', 'popfile')[pop_info['city']])
    file_name = os.path.basename(pop_file).split('.')[0]
    dir_name  = os.path.dirname(pop_file)
    pickle_file = os.path.normpath(dir_name + '/' + file_name + '.pickle')
    # if there is already a pickle with the same name get it
    if (os.path.isfile(pickle_file)):
        if verbose:
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Found population pickle! Using it: {pickle_file}")
        pop_df = pd.read_pickle(pickle_file)
    else:
        pop_df = pd.read_csv(pop_file)
        pop_df.to_pickle(pickle_file)
        if verbose:
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Population pickle saved to {pickle_file}")

    if mode == 'generator':
        if verbose:
            print("get_population_dictionary: generator type: {}".format(gentype))
        if pop_info['size'] <= 0:
            print("ERROR: get_population_dictionary, in generation mode, population size MUST be > 0", file=sys.stderr)
            sys.exit(111)
        tiles_to_use = sorted(set(pop_df['tile_id'].tolist()))
        pop = gen_population(in_path=in_path, territory=territory, pop_info=pop_info, tiles_to_use=tiles_to_use, gentype_coord=gentype, rng=rng, seed=seed, verbose=verbose)
    elif mode == 'reader':
        pop = read_population(in_path, pop_df, pop_info, territory, pop_filename=os.path.abspath(pop_file), read_type=(groups=='real'), rng=rng, seed=seed, verbose=verbose)
    else:
        print("unknown mode {}, abort!".format(mode), file=sys.stderr)
        sys.exit(111)

    return pop


def sort_pop_dict_arrays(order, pop):
    """
    Sort all data stored in the population dictionary (type, fitness, tile_id, coord) based on the given order.

    :param array order: the order to be used for sorting (as returned by a call to argsort) 
    :param dict pop: the population dict as returned by gen_population
    :return: the sorted population dict
    """

    pop['type']      = pop['type'][order]
    pop['fitness']   = pop['fitness'][order]
    pop['tile_id']   = pop['tile_id'][order]
    pop['household'] = pop['household'][order]
    x, y = zip(*pop['coord'])
    x = np.array(x)
    y = np.array(y)
    pop['coord'] = list(zip(x[order], y[order]))
    return pop


def count_tot_nodes_per_group(pop):
    """
    Count the number of nodes of each tile.

    :param dict pop: a dict as returned by gen_population
    :return: a dictionary {group0: N0, group1: N1, ...}
    """
    
    nodes_per_group = Counter(pop['type'])
    return nodes_per_group



###############################################################################
#
#                               DISTANCES
#
###############################################################################

def get_grid_centers (territory):
    """
    Compute the coordinates of the centers of each tile in the grid.
    
    :param dict territory: a dict as returned by build_projected_territory
    :return: a list of (x,y) coordinates, one for each tile; the tiles are enumerated bottom-to-top left-to-right 
    """
    # Get input var from territory 
    x0 , y0  = territory['origin'] 
    ntx, nty = territory['ntiles']
    tx , ty  = territory['tile']
    tx2 = tx/2.0
    ty2 = ty/2.0

    grid = [0]*ntx*nty
    
    x, y = x0, y0
    for j in range(nty):
        for i in range(ntx):
            ctile = (x + tx2, y + ty2)
            gid   = i + j*ntx
            grid[gid] = ctile
            x = x + tx
        y = y + ty
        x = x0
        
    return grid


def compute_grid_distances(origin_tile_id, grid_centers, dist_info): 
    """
    Compute distances between one of the tiles (called origin) and all other tiles.
    The distances are computed between the centers of the tiles.

    :param int origin_tile_id: id of the origin tile (the id of the tile in position (i,j) of the grid is i*n_x+j)
    :param list grid_centers: the list of the (x,y) coordinates of the center of all the tiles in the grid
    :param dict dist_info: a dictionary storing information about how to compute the distances (distance function, distance between a tile and itself) 
    :return: the numpy array of distances between the origin and all other tiles
    """

    zero_dist = dist_info['zero_dist']
    dtype     = dist_info['dist_func']

    x0, y0  = grid_centers[origin_tile_id]
    gcoords = np.asarray(grid_centers)
    gorigin = np.asarray((x0, y0)).reshape(1,2)
    
    if 'euclidean' in dtype:
        d = cdist(gorigin, grid_centers, metric='euclidean')[0]
        d[origin_tile_id] = zero_dist
    elif dtype == 'one':    
        d = np.array(np.full((1, len(grid_centers)), 1))[0]
    else:
        print("ERROR: compute_grid_distances: Unknown distance function! Abort", sys.stderr)
        sys.exit(111)
    return d


def preprocess_distances(distances, dist_info):
    '''
    Apply the function dist_func (D in the paper) to the given tile-to-tile distances.

    :param distances: the distances to which D must be applied
    :type distancs: list, array, int or float
    :param dict dist_info: a dictionary storing information about how to compute the distances (distance function, distance between a tile and itself) 
    :return: the numpy array of preprocessed distances
    '''

    if dist_info['dist_func'] == 'euclideanSquare':
        preprocessed_distances = np.square(distances)
    elif dist_info['dist_func'] == 'euclideanPower':
        preprocessed_distances = np.power(distances, float(dist_info['dist_power']))
    else:
        preprocessed_distances = distances
    return preprocessed_distances 


def get_grid_distances(tid1, tid2, ntx, nty, grid_distance_array_0):
    """
    Compute the distance between two given tiles, based on the array of distances between tile 0 (bottom-left) and all the other tiles, called grid_distance_array_0. 
    It leverages the fact that the distance between tile (i,j) and tile (i',j') equals the position (i'-i, j'-j) of grid_distance_array_0.
    See the function compute_grid_distances to see how to obtain grid_distance_array_0.

    :param int tid1: id of the first tile
    :param int tid2: id of the second tile
    :param int ntx: the number of tiles along the x axis of the grid
    :param int nty: the number of tiles along the y axis of the grid
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :return: the distance between the two tiles tid1, tid2
    """
    
    ty1 = tid1//ntx
    tx1 = tid1 - ntx*ty1
    ty2 = tid2//ntx
    tx2 = tid2 - ntx*ty2
    delta_x = abs(tx1-tx2)
    delta_y = abs(ty1-ty2)
    gid = delta_x + ntx*delta_y
    d = grid_distance_array_0[gid]
    return d


def get_distance_array(tid, territory, grid_distance_array_0):
    """
    Compute the distance between one tile and all others. It is a wrapper around get_grid_distances.

    :param int tid: id of the tile of interest
    :param dict territory: a dict as returned by build_projected_territory
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :return: the distance between the tile tid and all others
    """
    
    ntx, nty = territory['ntiles']
    nt = ntx*nty
    distance_array = np.array([get_grid_distances(tid, i, ntx, nty, grid_distance_array_0) for i in range(nt)])
    return distance_array


def get_distance_matrix(territory, grid_distance_array_0):
    """
    Compute the distance between all tile pairs. It is a wrapper around get_grid_distances.

    :param dict territory: a dict as returned by build_projected_territory
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :return: a ntiles x ntiles matrix of distances between all tile pairs
    """
    
    ntx, nty = territory['ntiles']
    nt = ntx*nty
    distance_matrix = np.array([[get_grid_distances(i, j, ntx, nty, grid_distance_array_0) for j in range(nt)] for i in range(nt)])
    return distance_matrix


def get_one_node_distance_array(n, grid_distance_array_0, territory, pop_tile_id_array, zero_dist):
    """
    Compute the distance between one node of the population and all others, where the distance between two nodes is estimated as the distance between the centers of their tiles.

    :param int n: the id of the node of interest
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :param dict territory: a dict as returned by build_projected_territory
    :param array pop_tile_id_array: array of tile ids of all nodes of the population
    :param float zero_dist: the distance between a tile and itself
    :return: the array of distances between node n and all other nodes
    """
    
    # Compute distance coefficient
    my_tile_id = pop_tile_id_array[n]
    # Compute distance among grid centers
    tile_distances = get_distance_array(my_tile_id, territory, grid_distance_array_0)
    # Assign distances to all the points
    distance_array = tile_distances[pop_tile_id_array]
    return distance_array



###############################################################################
#
#                                HOUSEHOLDS
#
###############################################################################

def compute_households(in_path, bash_command='BuildSBG/bf', dump_filename='households_output.txt', pop_filename='', seed=0, verbose=False):
    '''
    Compute the household structure by running a specific executable.

    :param str in_path: path to the root folder of all input data
    :param str bash_command: the path (relative to in_path) of the executable that computes the households
    :param str dump_filename: the base-name for the temporary file where the household structure is temporary printed
    :param str pop_filename: the file that stores the population in csv format  
    :param int seed: seed for the random number generator to be used to generate the household structure
    :param bool verbose: whether to print information about the output of the computation
    :return: a numpy array of ints of length N that represent the household id of each node of the population (or None if the executable is not found)
    '''

    bash_command = os.path.join(in_path, bash_command)
    if not os.path.exists(bash_command):
        print('compute_households: error! file {} not found! you might need to run {} first. The model will NOT include households!'.format(bash_command, bash_command.rsplit('/',1)[0]+'/make'))
        return None
    bash_command += ' -c BF.ini'
    bash_command += ' -s ' + os.path.join(in_path, 'BuildSBG')
    while True:
        temp_filename = dump_filename.rsplit('.',1)[0]+'_{}.txt'.format(np.random.randint(100000))
        if not os.path.exists(temp_filename):
            dump_filename = temp_filename
            break
    if pop_filename:
        bash_command = bash_command + ' -p ' + pop_filename + ' -d ' + str(seed)
    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Households, bash command:\n\t{bash_command}")
    with open(dump_filename, 'w') as fout:
        subprocess.run(bash_command, stdout=fout, shell=True)
    colnames = ['index', 'household', 'done'] # 'tile', 'age', 'role', 'done', 'parent', 'peer', 'f1', 'f2', 'f3']
    hh_df = pd.read_csv(dump_filename, usecols=[0,1,5], names=colnames, header=None, sep=' ')
    undone=hh_df['done'].isin([0]).sum()
    totnum=hh_df.shape[0]
    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Households, undone: {undone/totnum}")
    os.remove(dump_filename)
    hh = hh_df.sort_values('index')['household'].to_numpy(np.int)
    return hh 


def extract_households(hh, sample_size=0, rng=None):
    # this assumes that hh is already sorted by population index
    '''
    Extract the households as a list of arrays of individual ids, based on the array of household ids returned by compute_households.
    A sample of households can be obtained that involves a total of sample_size individuals.

    :param array hh: a numpy array of ints of length N that represent the household id of each node of the population, as returned by compute_households
    :param int sample_size: if >0, the size of the sample of the population that must be extracted (a sufficient number of households whose total population is >=sample_size is extracted)
    :param rng: random number generator to be used for the population
    :return: the households as a list of arrays of ids; the ids in the same array are the individuals in the same household
    '''

    hh = np.asarray(hh)
    order = hh.argsort()
    keys, values = hh[order], order
    start = np.searchsorted(keys,0)
    keys = keys[start:]
    values = values[start:]
    ukeys, index = np.unique(keys, True)
    arrays = np.split(values, index[1:])
    if 0<sample_size:
        rng    = default_rng(rng)
        sorter = rng.permutation(len(arrays))
        arrays = np.asarray(arrays)[sorter] 
        idx    = np.searchsorted(np.cumsum([len(x) for x in arrays]),sample_size)
        arrays = arrays[:idx+1]
    return arrays



###############################################################################
#
#                         AGE-MIXING PROBABILITIES
#
###############################################################################

def preprocess_g2g_matrix(g2g_dict, pop_info, pop):
    '''
    Apply the preprocessing to the age-mixing matrix S, here stored as a group-to-group dictionary g2g_dict.

    :param dict g2g_dict: a dictionary where the contacts between group k and group h is stored as g2g_dict[k][h]
    :param dict pop_info: a dict as returned by read_pop_info 
    :param dict pop: a dict as returned by gen_population
    :return: the matrix S as a n_groups x ngroups numpy array
    '''

    # Transform dict to matrix
    nodes_per_group = count_tot_nodes_per_group(pop)
    group_names = pop_info['group_names']
    mgp_matrix = np.array([[g2g_dict[k][h] for h in group_names] for k in group_names])

    # Impose reciprocity based on current population
    n = len(group_names)
    group_size_array = np.array([nodes_per_group[i] for i in range(n)])

    g=np.zeros((n,n))
    for i in range(0,n):
        for j in range(0,n):
            if i!=j:
                g[i][j] = 0.5*mgp_matrix[i][j]/group_size_array[j] + 0.5*mgp_matrix[j][i]/group_size_array[i]
            if i == j:
                g[i][i] = mgp_matrix[i][i]/(group_size_array[i]-1)

    mgp_matrix=g
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Builds groups probability array, input g2g matrix normalized:\n {mgp_matrix}")

    return mgp_matrix


def get_one_node_group_probability_array(u, groups_prob_array, ngroups, pop_type_array):
    '''
    Returns the age-mixing coefficient s_{g_u,g_v} for a given node u and all other nodes v, as an array of length N
    
    :param int u: the id of the node of interest
    :param array groups_prob_array: the matrix S of age-mixing coefficients, as a 1d array of length ngroups**2
    :param int ngroups: the number of age-groups in the population
    :param array pop_type_array: the array of the age-group of all nodes of the population
    :return: the array of age-mixing coefficients s_{g_u,g_v} for v=0,...,N-1
    '''

    # Build the array of indices
    idx_array = pop_type_array + ngroups*pop_type_array[u]
    # Get the probability array for the node n
    prob_array = groups_prob_array[idx_array]
    return prob_array



###############################################################################
#
#                             EDGE PROBABILITIES
#
###############################################################################

def pairs_counter_matrix(group_size_array):
    """
    Compute the number m_{i,j} of distinct pairs of nodes in two groups i,j as a ngroups x ngroups numpy array
    
    :param array group_size_array: the number of nodes of each group, as a 1d array of length ngroups
    :return: a upper triangle (all values below the diagonal are set to 0) array storing the values m_{i,j}
    """
    
    # Set M[i][j] = |g_i|*|g_j| if i!=j, (|g_i|*(|g_i|-1) if i==j 
    M = np.outer(group_size_array,group_size_array)-np.diag((np.square(group_size_array)+group_size_array)/2)
    M = np.triu(M)
    return M 


def compute_average_attractivity(M, pop, grid_distance_array_0, territory):
    """
    Compute the average attractivity A(i,j) for all pair of groups i,j as a ngroups x ngroups numpy array

    :param array M: the matrix of coefficients m_{i,j} as returned by pairs_counter_matrix
    :param dict pop: a dict as returned by gen_population
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :param dict territory: a dict as returned by build_projected_territory
    :return: the ngroups x ngroups array of average attractivity (A(i,j) is set to 1 if the attractivity is 0 to avoid errors afterward)
    """
    
    ntx, nty = territory['ntiles']
    nt = ntx*nty
    ng = M.shape[0]
    tile_distance_matrix = get_distance_matrix(territory, grid_distance_array_0)
    tile_inverse_distance_matrix = 1/tile_distance_matrix
    # Initialize A[i,j] = sum_{u in g_i, v in g_j, u != v} D(u,v)*f(u)*f(v)
    A = np.zeros((ng,ng))
    for gid1 in range(ng):
        group1 = np.where(pop['type']==gid1)[0]
        # this cycle is needed to avoid memory errors for cartesian products of large groups
        for idx,i in enumerate(group1):
            tile = pop['tile_id'][i]
            inverse_distances = tile_inverse_distance_matrix[tile][pop['tile_id'][group1[idx+1:]]]
            fitness_products  = pop['fitness'][i]*pop['fitness'][group1[idx+1:]]
            A[gid1,gid1] += (inverse_distances*fitness_products).sum()
            for gid2 in range(gid1+1,ng):
                in_group2 = pop['type']==gid2
                inverse_distances = tile_inverse_distance_matrix[tile][pop['tile_id'][in_group2]]
                fitness_products  = pop['fitness'][i]*pop['fitness'][in_group2]
                A[gid1,gid2] += (inverse_distances*fitness_products).sum()
    # Divide A[i,j] by M[i,j]
    A = A/np.where(M>0,M,1)
    return A


def get_consistency_value(A, g2g_matrix, pop, territory, grid_distance_array_0):
    '''
    Compute the min_{u,v} {A(g_u,g_v)/(M(g_u,g_v)*a(u,v))} used to verity that the chosen average number of friends mu is consistent with the imposed age-mixing

    :param array A: the array of average attractivities as returned by compute_average_attractivity
    :param array g2g_matrix: the matrix S as a n_groups x ngroups numpy array, as returned by preprocess_g2g_matrix
    :param dict pop: a dict as returned by gen_population
    :param dict territory: a dict as returned by build_projected_territory
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :return: min_{u,v} {A(g_u,g_v)/(M(g_u,g_v)*a(u,v))} 
    '''

    tile_distance_matrix = get_distance_matrix(territory, grid_distance_array_0)
    tile_inverse_distance_matrix = 1/tile_distance_matrix
    N = len(pop['type'])
    m = np.inf
    group_attractivity = A/g2g_matrix
    for i in range(N-1):
        tile = pop['tile_id'][i]
        inverse_distances = tile_inverse_distance_matrix[tile][pop['tile_id'][i+1:]]
        fitness_products  = pop['fitness'][i]*pop['fitness'][i+1:]
        group = pop['type'][i]
        g2g_attractivity  = group_attractivity[group][pop['type'][i+1:]] 
        temp_m = (g2g_attractivity/(inverse_distances*fitness_products)).min()
        if temp_m < m:
            m = temp_m
    return m


def compute_normalization_matrix(mu, pop, group_size_array, g2g_matrix, grid_distance_array_0, territory): 
    """
    Compute the normalization coefficients for the edge probability formula as a ngroups x ngroups array H.

    :param int mu: the average degree of the friendship graph
    :param dict pop: a dict as returned by gen_population
    :param array group_size_array: the number of nodes of each group, as a 1d array of length ngroups
    :param array g2g_matrix: the matrix S as a n_groups x ngroups numpy array, as returned by preprocess_g2g_matrix
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :param dict territory: a dict as returned by build_projected_territory
    :return: the normalization coefficients in a ngroups x ngroups numpy array H
    """
    N = len(pop['type'])
    M = pairs_counter_matrix(group_size_array)
    # Compute K = mu*N/2 / sum(S(g_i,g_j)*M(g_i,g_j)) 
    K = mu*N/(2*(M*g2g_matrix).sum())
    A = compute_average_attractivity(M, pop, grid_distance_array_0, territory)
    # Check consistency
    m = get_consistency_value(A, g2g_matrix, pop, territory, grid_distance_array_0)
    max_mu = m*2*(M*g2g_matrix).sum()/N
    print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - The condition to avoid inconsistencies is mu<={max_mu}")
    #print('The condition to avoid inconsistencies is mu<={}'.format(max_mu))
    if K<=m:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Consistency condition is met! No probabilities greater than 1 are expected!")
    else:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [WARNING] Consistency condition violated, probabilities greater than 1 are expected!")
    H = K/np.where(A>0,A,1)
    return H


def compute_single_node_edge_probability(u, pop, mu, distance_array, normalized_g2g_matrix, count_errors=False):
    """
    Compute the probability of all adges (u,v) for fixed u and v>u, based on equation (2.2).

    :param int u: the id of the node of interest
    :param dict pop: a dict as returned by gen_population
    :param int mu: the average degree of the friendship graph
    :param array distance_array: array of (preprocessed) distances between u and all other nodes, as returned by get_one_node_distance_array
    :param array normalized_g2g_matrix: the matrix S after element-wise normalization by the matrix returned by compute_normalization_matrix, as a n_groups x ngroups numpy array
    :param bool count_errors: whether to count and return the number of edges having probability >1 (this is possible if mu is above the threshold (2.6))
    :return: a numpy array of probabilities of length N-u, where N is the size of the population
    """

    N = len(pop['type'])
    ngroups   = normalized_g2g_matrix.shape[0]
    g2g_array = normalized_g2g_matrix.reshape(-1)

    # retrieve the correct element of groups_prob_array for each other node with respect to n:
    single_node_gp_array = get_one_node_group_probability_array(u, g2g_array, ngroups, pop['type'])
   
    # Add all things up
    edges_probability = pop['fitness'][u] * pop['fitness'][u+1:] * single_node_gp_array[u+1:] / distance_array[u+1:]
    
    if count_errors:
        errors = (edges_probability>1).sum()
        return edges_probability, errors
    else:
        return edges_probability



###############################################################################
#
#                               GRAPH CONSTRUCTION
#
###############################################################################

def get_graph(pop, pop_info, grid_distance_array_0, territory, dist_info, g2g_matrix, mu, rng=None, verbose=False):
    '''
    Construct the urban social graph based on all available data and all configuration parameters 

    :param dict pop: a dict as returned by gen_population
    :param dict pop_info: a dict as returned by read_pop_info 
    :param array grid_distance_array_0: array of distances between tile 0 and all other tiles, as returned by compute_grid_distances
    :param dict territory: a dict as returned by build_projected_territory
    :param dict dist_info: a dictionary storing information about how to compute the distances (distance function, distance between a tile and itself) 
    :param array g2g_matrix: the matrix S as a n_groups x ngroups numpy array, as returned by preprocess_g2g_matrix
    :param int mu: the average degree of the friendship graph
    :param rng: random number generator to be used for the population
    :type rng: Generator or int or None
    :param bool verbose: whether to print information about the graph during the construction
    :return: the igraph Graph object representing the urban social graph
    '''

    # number of individuals
    N = pop_info['size']
    # number, names and sizes of groups 
    group_names = pop_info['group_names']
    ngroups     = len(group_names)
    nodes_per_group  = count_tot_nodes_per_group(pop)
    group_size_array = np.array([nodes_per_group[i] for i,k in enumerate(group_names)])
    # number of tiles in the grid
    ntx, nty = territory['ntiles']
    ntiles   = ntx*nty
    # intra-tile distance and distance function
    zero_dist = dist_info['zero_dist']
    dist_func = dist_info['dist_func']

    ##### HOUSEHOLDS #####
    hh = pop['household']
    if hh is None:
        edges = []
        layer = []
        distances = []
        if verbose:
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - No households given") 
    else:
        hh = extract_households(hh, rng=rng) 
        
        # households:
        edges = [e for hh_ in hh for e in combinations(hh_,2)]
        layer = ['household']*len(edges)
        if 'euclidean' in dist_func:
            distances = [zero_dist]*len(edges)
        elif dist_func == 'one':    
            distances = [1]*len(edges)
        else:
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - [ERROR] Unknown distance function! Abort", sys.stderr)
            sys.exit(111)
        if verbose:
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Number of households: {len(hh)}") 
            print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Total number of household edges is {len(edges)}")
    ######################

    ##### FRIENDSHIP #####
    # Preprocess grid distances
    preprocessed_grid_distance_array_0 = preprocess_distances(grid_distance_array_0, dist_info)
    preprocessed_zero_dist = preprocess_distances(zero_dist, dist_info)
    # Normalize group probability coefficients; normalization is computed -per group- thus we multiply it by mgp NOW
    H = compute_normalization_matrix(mu, pop, group_size_array, g2g_matrix, preprocessed_grid_distance_array_0, territory) 
    normalized_g2g_matrix = g2g_matrix*H 
    
    tot_probability = 0

    tot_err=0
    for i in range(0, N):
        # get preprocessed distances for node i
        dist_array = get_one_node_distance_array(i, preprocessed_grid_distance_array_0, territory, pop['tile_id'], preprocessed_zero_dist)
        # compute edge probabilities 
        ep = compute_single_node_edge_probability(i, pop, mu, dist_array, normalized_g2g_matrix, count_errors=verbose)
        if verbose:
            ep, err = ep 
            tot_err += err
        # extract and store edges
        rng = default_rng(rng)
        to_connect = ep > rng.uniform(0, 1, ep.shape)
        edges.extend([(i,i+1+c) for c in np.where(to_connect)[0].tolist()])
        # retrieve and store original distances
        dist_array = get_one_node_distance_array(i, grid_distance_array_0, territory, pop['tile_id'], zero_dist)
        dist_array = dist_array[i+1:]
        distances.extend(dist_array[to_connect].tolist())
        
        tot_probability += ep.sum()

    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Imposed average degree is {mu}, obtained expected average degree is {tot_probability*2/pop_info['size']}")

    layer += ['friendship']*(len(edges)-len(layer))
    ######################
    
    g = ig.Graph(N, edges)
    g.vs['tile_id'] = pop['tile_id']
    g.vs['type'] = pop['type']
    g.vs['household'] = pop['household']
    g.es['layer'] = layer
    g.es['distance'] = distances
    g.simplify(combine_edges={'layer':'concat', 'distance':'first'})

    if verbose:
        print(f"{datetime.now().strftime('%d/%m/%Y %H:%M:%S')} - Edges, tot: {g.ecount()}, p>1: {tot_err}, %p>1: {round(100*tot_err/g.ecount(),5)}")

    return g 







def create_sb_graph(in_path, config_file, sort_by=None, rng=None, city=''):

    data      = get_input_data_structures(in_path, config_file, city=city)
    territory = data['territory']
    pop_info  = data['pop_info']
    dist_info = data['dist_info']
    mu        = read_from_config(config_file, 'SOCIALITY', 'avg_deg')
    g2g_type  = read_from_config(config_file, 'SOCIALITY', 'group2group_type')

    print('create_sb_graph: selected city is {}'.format(pop_info['city']))

    ##### Compute grid tiles distances #####
    ##### NOTE: from grid_distance_array_0 you can get every distance between two tiles (d(t1, t2))
    ##### by means of the function get_grid_distances(tid1, tid1, distance_array_0)
    ########################################
    # Compute the coordinates of the grid centers
    grid_centers = get_grid_centers(territory)
    # Compute tile_0 distance array: grid_distance_array_0
    grid_distance_array_0 = compute_grid_distances(0, grid_centers, dist_info)
    
    # Create POP
    start = timer()
    pop = get_population_dictionary(in_path, config_file, territory, pop_info, rng=rng)
    end = timer()
    print('create_sb_graph: pop time = {}'.format(end - start))
    
    # check if size is consistent 
    if pop_info['size'] != len(pop['coord']):
        print('create_sb_graph: setting pop_info["size"] to', len(pop['coord']))
        pop_info['size'] = len(pop['coord'])

    # Sort population arrays by type
    if sort_by:
        order = pop[sort_by].argsort()
        sorted_pop = sort_pop_dict_arrays(order, pop)
    
    # STEFANO: removed the following and added preprocessing right after g2g_dict is read from config
    # build the mixing group probability matrix
    # if g2g_dict == 'constant':
    #     group_names = pop_info['group_names']
    #     g2g_dict = {k:{h:1 for h in group_names} for k in group_names}
    # g2g_matrix = preprocess_g2g_matrix(g2g_dict, pop_info, pop)
    if g2g_type == 'constant':
        group_names = pop_info['group_names']
        # g2g_dict = {k:{h:1 for h in group_names} for k in group_names}
        n = len(group_names)
        g2g_matrix = np.ones((n,n))
    else:
        g2g_dict = read_from_config(config_file, 'SOCIALITY', 'group2group')
        g2g_matrix = preprocess_g2g_matrix(g2g_dict, pop_info, pop)

    # Create Graph new
    start = timer()
    g_sb = get_graph(pop, pop_info, grid_distance_array_0, territory, dist_info, g2g_matrix, mu, rng=rng)
    end = timer()
    print('create_sb_graph: graph time = {}'.format(end - start))
    print('create_sb_graph: graph created. number of nodes: {}, number of edges: {}'.format(g_sb.vcount(), g_sb.ecount()))

    return pop, g_sb, grid_distance_array_0
    

