import os
import pickle
import igraph
import numpy as np
import pandas as pd
import seaborn as sns
from collections import Counter
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

def parse_path(path):
    values = path.split('_')
    params = [r'$\Gamma$', r'$\delta t$', r'$\beta', r'$\Pi$', 'M', r'$\epsilon$', r'$\phi$', r'$d_{\min}$', r'$s_{\max}$', r'$d_{\mathrm{link}}$']
    return {k:v for k,v in zip(params,values)}

def import_vertex_attributes(g1, g2, attributes):
    for att in attributes:
        values = g2.vs[att]
        g1.vs[att] = [values[name] for name in g1.vs['name']]

# data_df = {}

#main_path = '/home/stefano/Ricerca/Code/realplaces/webGUI/static'
main_path = '../webGUI/static'

paths = {0:main_path} # paths = {i:os.path.join(main_path,str(i)) for i in range(10)}
# par_dict = parse_path(main_path)
# relevant_params = [r'$\Gamma$', 'M', r'$d_{\min}$', r'$d_{\mathrm{link}}$']
data = {} # data = {k:[] for k in relevant_params}
data['time slot'] = []
data['inf_0'] = []
data['infected'] = []
data['area type'] = []
data['inter-group contacts'] = []
data['total contacts'] = []
data['run'] = []
for h,p in paths.items():
    print(f'at {p}')
    with open(os.path.join(p,'daily_graphs.pickle'),'rb') as f:
        daily_graphs = pickle.load(f)
    with open(os.path.join(p,'one_day_epidemic_runs.pickle'), 'rb') as f:
        epi_runs = pickle.load(f)[0]
    for i,(g,inf) in enumerate(zip(daily_graphs,epi_runs)):
        layers_all = Counter(g.es['layer'])
        inter_group_edges = [e['layer'] for e in g.es if g.vs['group'][e.source]!=g.vs['group'][e.target]]
        layers_inter = Counter(inter_group_edges)
        inf_0 = len(inf['infected_0'])
        inf_1 = len(inf['infected_1'])
        for l,v in layers_all.items():
            data['run'].append(h)
            data['area type'].append(l)
            data['total contacts'].append(v)
            data['inter-group contacts'].append(layers_inter.get(l,0))
            data['time slot'].append(i)
            data['inf_0'].append(inf_0)
            data['infected'].append(inf_1)
            # for k in relevant_params:
            #     data[k].append(par_dict[k])

data_df = pd.DataFrame(data) #data_df[main_path] = pd.DataFrame(data)

layer_order = sorted(set(data_df['area type'])) # layer_order = sorted(set(data_df[main_path]['area type']))
palette = dict(zip(layer_order, sns.color_palette(n_colors=len(layer_order))))

fig, ax = plt.subplots(1,2, figsize=(10,4))

# sns.set_context('talk')
# plt.figure(figsize=(9,5))
sns.lineplot(data=data_df, x='time slot', hue='area type', hue_order=layer_order, y='total contacts', palette=palette, ax=ax[0])
ylim = ax[0].get_ylim()
ax[0].legend(loc='upper left')
# plt.savefig(os.path.join(main_path,'total_contacts.png'), bbox_inches='tight')

# sns.set_context('talk')
# plt.figure(figsize=(9,5))
sns.lineplot(data=data_df, x='time slot', hue='area type', hue_order=layer_order, y='inter-group contacts', palette=palette, ax=ax[1])
ax[1].set_ylim(ylim)
ax[1].legend(loc='upper left')
# plt.savefig(os.path.join(main_path,'inter_group_contacts_ylim.png'), bbox_inches='tight')

ax[0].set_title('TOTAL CONTACTS')
ax[1].set_title('INTER-GROUP CONTACTS')
plt.savefig(os.path.join(main_path, 'contacts.png'), bbox_inches='tight')

sns.set_context('talk')
plt.figure(figsize=(6,3))
# labels = {'nopref_15min_2_1000_5_0_05_1m_05_2m':'benchmark',
#           'pref_15min_2_1000_5_0_05_1m_05_2m':'preferences',
#           'nopref_15min_2_1000_5_0_05_1.5m_05_2m':'distancing'}
# for k,v in data_df.items():
#     if k in labels:
h = data_df.groupby(['run','time slot'])['infected'].mean().groupby('run').cumsum()
h = h.reset_index().rename(columns={'infected':'cumulative infections'})
sns.lineplot(data=h, x='time slot', y='cumulative infections') #, label=labels[k])
# plt.legend(loc='upper left')
plt.savefig(os.path.join(main_path, 'infected.png'), bbox_inches='tight')


#USG = igraph.read('/home/stefano/Ricerca/Code/realplaces/data/graphs/firenze/EP2/sb_graph_new.pickle')
USG = igraph.read('../data/graphs/firenze/EP2/sb_graph_new.pickle')
coords = {}
tile_infections = []
for h,p in paths.items():
    print(f'at {p}')
    with open(os.path.join(p,'daily_graphs.pickle'),'rb') as f:
        daily_graphs = pickle.load(f)
    with open(os.path.join(p,'one_day_epidemic_runs.pickle'), 'rb') as f:
        epi_runs = pickle.load(f)[0]
    g_ = []
    inf_0 = set()
    inf_1 = set()
    for i,(g,inf) in enumerate(zip(daily_graphs,epi_runs)):
        att = g.vs['coords']
        coords[i] = {vid:v for vid,v in enumerate(att) if v is not None}
        del(g.vs['coords'])
        import_vertex_attributes(g,USG,USG.vertex_attributes())
        g_.append(g)
        inf_0 = inf_0.union(set(inf['infected_0']))
        inf_1 = inf_1.union(set(inf['infected_1']))
    G = igraph.union(g_, byname=True)   
    tile = USG.vs['tile_id']
    inf = {}
    inf[0] = Counter([tile[n] for n in inf_0])
    inf[1] = Counter([tile[n] for n in inf_1])
    tile_infections.append(inf)

dimx = 15
dimy = 12
dim = dimx*dimy
c0 = []
c1 = []
for inf in tile_infections:
    c0_ = np.zeros(dim)
    c1_ = np.zeros(dim)
    for k,v in inf[0].items():
        c0_[k] = v
    for k,v in inf[1].items():
        c1_[k] = v
    c0_ = c0_.reshape(dimy,dimx)
    c1_ = c1_.reshape(dimy,dimx)
    c0.append(c0_)
    c1.append(c1_)
c0_mean = np.mean(c0,axis=0)
c1_mean = np.mean(c1,axis=0)

parco_tile = 95
t = np.zeros(dim)
t[parco_tile] = 1
t = t.reshape((dimy,dimx))
# ax = sns.heatmap(t)
# ax.invert_yaxis()

fig, ax = plt.subplots(1,2, figsize=(14,6))

f = sns.heatmap(c0_mean, vmin=0, vmax=c0_mean.max(), square=True, ax=ax[0])
# ax = f.ax_heatmap
f.add_patch(Rectangle(np.where(t>0)[::-1], 1, 1, fill=False, edgecolor='blue', lw=3))
# plt.savefig(os.path.join(main_path, 'infections_heatmap_pre.png'), bbox_inches='tight')

f = sns.heatmap(c1_mean, vmin=0, vmax=c1_mean.max(), square=True, ax=ax[1])
# ax = f.ax_heatmap
f.add_patch(Rectangle(np.where(t>0)[::-1], 1, 1, fill=False, edgecolor='blue', lw=3))

ax[0].set_title('BEFORE')
ax[1].set_title('AFTER')
plt.savefig(os.path.join(main_path, 'infections_heatmap.png'), bbox_inches='tight')

with open(os.path.join(main_path,'readyPlots'), 'w') as f:
    f.write('Plots are ready!')
