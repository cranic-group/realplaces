#ifndef __UTILS_H__
#define __UTILS_H__

#ifdef __cplusplus
#define UTILS_LINKAGE "C"
#else
#define UTILS_LINKAGE
#endif

extern UTILS_LINKAGE void *Malloc(size_t sz);
extern UTILS_LINKAGE void Free(void **ptr);
extern UTILS_LINKAGE void *Realloc(void *ptr, size_t sz);
extern UTILS_LINKAGE FILE *Fopen(const char *path, const char *mode);
extern UTILS_LINKAGE size_t Fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
extern UTILS_LINKAGE size_t Fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
extern UTILS_LINKAGE int Remove(const char *pathname);
extern UTILS_LINKAGE off_t getFsize(const char *fpath);
extern UTILS_LINKAGE double Wtime(void);
extern UTILS_LINKAGE void writelog(int, int, const char *, ...);
extern UTILS_LINKAGE char *Strdup(const char *);

#if !defined(TRUE)
enum {FALSE, TRUE};
#endif

#define SKIPBLANK         while(po[0] & (po[0]==' ' || po[0]=='\t')) po++; \
                          if (!po[0]) { \
                             i++;      \
                             po=argv[i]; \
                          }
  
enum RC{OK, APPLICATION_RC, STRDUP_RC, MALLOC_RC, FOPEN_RC, POPEN_RC, FGETS_RC,
        SOCKET_RC, SEND_RC, RECV_RC, LOCK_RC, UNLOCK_RC, MAKEMATR_RC,
        MAKEVECT_RC, FINDLOG_RC, FREAD_RC, FSTAT_RC, MMAP_RC,
        CHECKSFAILURE, TOOMANYATF};

#define MAXINPUTLINE 10000
#define MAXSTRLEN 1024
#define MAXFILELENGTH 256
#define INVALID_INT -999999
#define INVALID_REAL -1.e7
#define INVALID_STR NULL

#define READINTFI(v,s) snprintf(key,sizeof(key),"%s:%s","INPUTDATA",(s)); \
                          (v)=iniparser_getint(ini, key, INVALID_INT); \
                          if((v)==INVALID_INT) { \
                            writelog(TRUE,APPLICATION_RC, \
                                     "Invalid value <%d> for key <%s> from input file %s\n", \
                                     (v), key, ConfFileName);		\
                          }

#define READREALFI(v,s)  snprintf(key,sizeof(key),"%s:%s","INPUTDATA",(s)); \
                            (v)=(REAL)iniparser_getdouble(ini, key, INVALID_REAL);\
                            if((v)==(REAL)INVALID_REAL) { \
                              writelog(TRUE,APPLICATION_RC,\
                                       "Invalid value for key <%s> from input file %s\n",\
                                       key, ConfFileName); \
                            }

#define READSTRFI(v,s)  snprintf(key,sizeof(key),"%s:%s","INPUTDATA",(s)); \
                        { char* temp=iniparser_getstring(ini, key, INVALID_STR);		\
                           if(temp==INVALID_STR) { \
                                  writelog(TRUE,APPLICATION_RC,\
                                        "Invalid value for key <%s> from input file %s\n",\
                                        key, ConfFileName); \
                           }else{ strncpy((v),temp,MAXSTRLEN); /* sscanf(temp,"%s",(v)); */ }}

#endif

void **makematr(int, int, int);

void freematr(int r, void **matr);

