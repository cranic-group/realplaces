#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>
#include "extern.h"
#include "utils.h"

void *Malloc(size_t sz) {

	void *ptr;

	if (!sz) {
		printf("Allocating zero bytes...\n");
		exit(EXIT_FAILURE);
	}
	ptr = (void *)malloc(sz);
	if (!ptr) {
		fprintf(stderr, "Cannot allocate %zu bytes...\n", sz);
		exit(EXIT_FAILURE);
	}
	memset(ptr, 0, sz);
	return ptr;
}

void Free(void **ptr) {

	if (*ptr) {
		free(*ptr);
		*ptr = NULL;
	}
	return;
}

void *Realloc(void *ptr, size_t sz) {

        void *lp;

	if (!sz) {
		printf("Re-allocating to zero bytes, are you sure you want this?\n");
	}
        lp = (void *)realloc(ptr, sz);
        if (!lp && sz) {
                fprintf(stderr, "Cannot reallocate to %zu bytes...\n", sz);
                exit(EXIT_FAILURE);
        }
        return lp;
}

FILE *Fopen(const char *path, const char *mode) {

        FILE *fp = NULL;
        fp = fopen(path, mode);
        if (!fp) {
                fprintf(stderr, "Cannot open file %s...\n", path);
                exit(EXIT_FAILURE);
        }
        return fp;
}

size_t Fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream) {

	size_t wmemb=0;

	wmemb = fwrite(ptr, size, nmemb, stream);
	if (wmemb < nmemb) {
		fprintf(stderr, "Error while writing to file!\n");
		exit(EXIT_FAILURE);
	}
	return wmemb;
}

size_t Fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {

	size_t rmemb=0;

	rmemb = fread(ptr, size, nmemb, stream);
	if (rmemb < nmemb && ferror(stream)) {
		fprintf(stderr, "Error while reading from file, could not read more than %zu elements!\n", rmemb);
		exit(EXIT_FAILURE);
	}
	return rmemb;
}

int Remove(const char *pathname) {

	int rv = remove(pathname);
	if (rv && errno != ENOENT) {
		fprintf(stderr, "Error removing file %s: %s\n", pathname, strerror(errno));
		exit(EXIT_FAILURE);
	}
	return rv;
}

off_t getFsize(const char *fpath) {

        struct stat     st;
        int             rv;

        rv = stat(fpath, &st);
        if (rv) {
                fprintf(stderr, "Cannot stat file %s...\n", fpath);
                exit(EXIT_FAILURE);
        }
        return st.st_size;
}

double Wtime(void) {
	struct timespec tp;

	int rv = clock_gettime(CLOCK_MONOTONIC, &tp);
	if(rv) return 0;

	return tp.tv_nsec/1.0E+9 + (double)tp.tv_sec;
}

/* matrix allocation */
void **makematr(int r, int c, int s) {
  int i;
  char **pc;
  short int **psi;
  int **pi;
  double **pd;

  switch(s) {
  case sizeof(char):
    pc=(char **)malloc(r*sizeof(char *));
    if(!pc) writelog(TRUE, MAKEMATR_RC, "error in makematr 1\n");
    for(i=0; i<r; i++) {
      pc[i]=(char *)malloc(c*sizeof(char));
      if(!pc[i]) writelog(TRUE, MAKEMATR_RC, "error in makematr 2\n");
      memset(pc[i],0,c*sizeof(char));
    }
    return (void **)pc;
  case sizeof(short int):
    psi=(short int **)malloc(r*sizeof(short int*));
    if(!psi) writelog(TRUE, MAKEMATR_RC, "error in makematr 3\n");
    for(i=0; i<r; i++) {
      psi[i]=(short int *)malloc(c*sizeof(short int));
      if(!psi[i]) writelog(TRUE, MAKEMATR_RC, "error in makematr 4\n");
      memset(psi[i],0,c*sizeof(short int));
    }
    return (void **)psi;
  case sizeof(int):
    pi=(int **)malloc(r*sizeof(int*));
    if(!pi) writelog(TRUE, MAKEMATR_RC, "error in makematr 5\n");
    for(i=0; i<r; i++) {
      pi[i]=(int *)malloc(c*sizeof(int));
      if(!pi[i]) writelog(TRUE, MAKEMATR_RC, "error in makematr 6\n");
      memset(pi[i],0,c*sizeof(int));
    }
    return (void **)pi;
  case sizeof(double):
    pd=(double **)malloc(r*sizeof(double*));
    if(!pd) writelog(TRUE, MAKEMATR_RC, "error in makematr 7 for %d rows\n",r);
    for(i=0; i<r; i++) {
      pd[i]=(double *)malloc(c*sizeof(double));
      if(!pd[i]) writelog(TRUE, MAKEMATR_RC, "error in makematr 8 for %d cols\n",c);
      memset(pd[i],0,c*sizeof(double));
    }
    return (void **)pd;
    break;
  default:
    writelog(TRUE,MAKEMATR_RC,"Unexpected size: %d\n",s);
    break;
  }
  return NULL;
}

/* vector allocation */
void *makevect(int d, int s) {
  char *pc;
  short int *psi;
  int *pi;
  double *pd;

  switch(s) {
  case sizeof(char):
    pc=(char *)malloc(d*sizeof(char));
    if(!pc) writelog(TRUE, MAKEVECT_RC, "error in makevect 1\n");
    memset(pc,0,d*sizeof(char));
    return (void *)pc;

  case sizeof(short int):
    psi=(short int *)malloc(d*sizeof(short int));
    if(!psi) writelog(TRUE, MAKEVECT_RC, "error in makevect 2\n");
    memset(psi,0,d*sizeof(short int));
    return (void *)psi;

  case sizeof(int):
    pi=(int *)malloc(d*sizeof(int));
    if(!pi) writelog(TRUE, MAKEVECT_RC, "error in makevect 3\n");
    memset(pi,0,d*sizeof(int));
    return (void *)pi;

  case sizeof(double):
    pd=(double *)malloc(d*sizeof(double));
    if(!pd) writelog(TRUE, MAKEVECT_RC, "error in makevect 4\n");
    memset(pd,0,d*sizeof(double));
    return (void *)pd;
    break;

  default:
    writelog(TRUE,MAKEVECT_RC,"Unexpected size: %d\n",s);
    break;
  }
  return NULL;
}

/* free matrix: r = number of rows */
void freematr(int r, void **matr) {
    int i;

    for (i=0; i<r; i++) {
        free(matr[i]);
    }
    free(matr);
}

/* free vect */
void freevect(void *vect) {
    free(vect);
}

char *Strdup(const char *str) {
  char *p=strdup(str);
  if(p==NULL) {
    writelog(TRUE,STRDUP_RC,"error copying string %s\n",str);
  }
}

void writelog(int end, int rc, const char *fmt, ...) {
  static FILE *filelog=NULL;
  char buf[MAXSTRLEN+1];
  va_list ap;
  va_start(ap, fmt);
#ifdef  HAVE_VSNPRINTF
  vsnprintf(buf, MAXSTRLEN, fmt, ap);       /* safe */
#else
  vsprintf(buf, fmt, ap);                   /* not safe */
#endif
  if(filelog==NULL) {
    filelog=fopen(LogFileName,"w");
    if(filelog==NULL) {
      fprintf(stderr,"Could not open %s file for logging, using stderr\n",LogFileName);
      filelog=stderr;
    }
  }
  fputs(buf,filelog);
  fflush(filelog);
  if(end) {
    fclose(filelog);
    exit(rc);
  }
}
