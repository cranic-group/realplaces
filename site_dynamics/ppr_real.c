#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "utils.h"
#include "iniparser.h"
#include "dictionary.h"

#define MINARG 2
#define REAL double
#define NUMBEROFPROB 2
#define DISTMIN 1. //It's the minumum distance,if rigidity of a certain place is set to 0.
#define ONEHALF .5
void Usage(char *cmd) {
    fprintf(stdout,"Usage %s -c conffile [-s directory] [-n totalnumberofpeople] [-g groupsfilename] [-f neighboursfilename] [-v] [-p]\n",cmd);
    exit(1);
}

int verbose=FALSE;
int plot_progress=FALSE;
char LogFileName[MAXFILELENGTH]= "PPR_real.log";

enum {CHILDREN, YOUNG, ADULTS, ELDERLY, NUMBEROFAGES};

typedef struct people {unsigned int age; unsigned int id; int *preference; REAL posx; REAL posy; REAL velx; REAL vely; REAL accx; REAL accy; unsigned int groupleader; struct people *nextingroup; struct group *group;} people;

typedef struct group {people *groupfirst; unsigned int group_id; unsigned int time_in; unsigned int time_out; struct group *nextgroup; int done; unsigned int groupcnt; unsigned int move; unsigned int neighbours; unsigned int *nbarray;} group;

typedef struct {unsigned int kind; REAL posx; REAL posy; REAL sidex; REAL sidey; REAL distance; REAL area; unsigned int attempts; unsigned int capacity; unsigned int cnt; unsigned int rigid; group *firstgroup; group *lastgroup;} places;


#define DISTANCE(x1,y1,x2,y2) sqrt((((x1)-(x2))*((x1)-(x2)))+(((y1)-(y2))*((y1)-(y2))))
#define DELIMITER ","

unsigned int ReadNumberofPeopleFromFile(char *filename) {
    int i;
    unsigned int peoplecnt=0;
    unsigned int peopleingroup;
    char stbuf[MAXINPUTLINE], *token;
    FILE *fpeople = Fopen(filename, "r");
    if(fgets(stbuf, MAXINPUTLINE, fpeople)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read first line of file %s\n",filename); }
    while ((fgets(stbuf, MAXINPUTLINE, fpeople))) {
        token=strtok(stbuf,DELIMITER);
        token=strtok(NULL,DELIMITER);
        token=strtok(NULL,DELIMITER);
        token=strtok(NULL,DELIMITER);
        peopleingroup=atoi(token);
        peoplecnt+=peopleingroup;
    }
    fclose(fpeople);
    return peoplecnt;
}

unsigned int ReadNumberofLeadersFromFile(char *filename) {
    unsigned int groupscnt=0;
    char stbuf[MAXINPUTLINE], *token;
    FILE *fpeople = Fopen(filename, "r");
    if(fgets(stbuf, MAXINPUTLINE, fpeople)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read first line of file %s\n",filename); }
    while ((fgets(stbuf, MAXINPUTLINE, fpeople))) {
        groupscnt++;
    }
    fclose(fpeople);
    return groupscnt;
}

unsigned int ReadGroupsData(char *groupsfn, char *neighboursfn, people *aofpeople, group *aofgroups, unsigned int npeople, int **tp, int nodp, REAL xcenter, REAL ycenter, REAL xdim, REAL ydim, unsigned int *starttime, unsigned int *endtime) {
    int i;
    unsigned int peoplecnt=0;
    unsigned int groupscnt=0;
    char stbuf[MAXINPUTLINE], *token;
    unsigned int peopleingroup, iloop;
    people *ptp;
    group *ptgroup;
    FILE *fgroups = Fopen(groupsfn, "r");
    iloop=0;
    if(fgets(stbuf, MAXINPUTLINE, fgroups)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read first line of file %s\n",groupsfn); }
    while ((fgets(stbuf, MAXINPUTLINE, fgroups))) {
        token=strtok(stbuf,DELIMITER);
        aofgroups[groupscnt].group_id=atoi(token);
        token=strtok(NULL,DELIMITER);
        aofgroups[groupscnt].time_in=atoi(token);
        token=strtok(NULL,DELIMITER);
        aofgroups[groupscnt].time_out=atoi(token);
        if(iloop==0){
            *starttime=aofgroups[groupscnt].time_in;
            *endtime=aofgroups[groupscnt].time_out;
        } else {
            *starttime=fmin(*starttime,aofgroups[groupscnt].time_in);
            *endtime=fmax(*endtime,aofgroups[groupscnt].time_out);
        }
        token=strtok(NULL,DELIMITER);
        peopleingroup=atoi(token);
        aofgroups[groupscnt].groupcnt=peopleingroup;
        ptgroup=&(aofgroups[groupscnt]);
        aofgroups[groupscnt].groupfirst=&aofpeople[peoplecnt];
        aofgroups[groupscnt].done=-1;
        aofgroups[groupscnt].move=1;
        aofgroups[groupscnt].nextgroup=NULL;
        aofgroups[groupscnt].neighbours=0;
        for(i=0; i<peopleingroup; i++) {
            token=strtok(NULL,DELIMITER);
            aofpeople[peoplecnt].id=atoi(token);
            token=strtok(NULL,DELIMITER);      
            aofpeople[peoplecnt].age=atoi(token);
            aofpeople[peoplecnt].preference=(int *)Malloc(sizeof(int)*nodp);
            memcpy(aofpeople[peoplecnt].preference,tp[aofpeople[peoplecnt].age],sizeof(int)*nodp);
            if(i==0) {
                aofpeople[peoplecnt].posx=xcenter+((drand48()-0.5)*xdim);
                aofpeople[peoplecnt].posy=ycenter+((drand48()-0.5)*ydim);
                aofpeople[peoplecnt].groupleader=1;
            } else {
                REAL angle=M_PI*drand48();
                aofpeople[peoplecnt].posx=aofgroups[groupscnt].groupfirst->posx+DISTMIN*cos(angle);
                aofpeople[peoplecnt].posy=aofgroups[groupscnt].groupfirst->posy+DISTMIN*sin(angle);
                aofpeople[peoplecnt].groupleader=0;	
                ptp->nextingroup=&aofpeople[peoplecnt];
            }
            aofpeople[peoplecnt].velx=0.0;
            aofpeople[peoplecnt].vely=0.0;
            aofpeople[peoplecnt].accx=0.0;
            aofpeople[peoplecnt].accy=0.0;
            aofpeople[peoplecnt].group=ptgroup;
            ptp=&aofpeople[peoplecnt];
            peoplecnt++;
        }
        ptp->nextingroup=NULL;
        groupscnt++;
        iloop++;
    }
    if(peoplecnt!=npeople) {
        writelog(TRUE,APPLICATION_RC,"Number of people not consistent: %d %d\n",peoplecnt, npeople);
    }
    fclose(fgroups);
    FILE *fnb = Fopen(neighboursfn, "r");
    unsigned int groupindex;
    unsigned int nbindex, nb;
    if(fgets(stbuf, MAXINPUTLINE, fnb)==NULL) { writelog(TRUE,APPLICATION_RC,"Could not read first line of file %s\n",neighboursfn); }
    while ((fgets(stbuf, MAXINPUTLINE, fnb))) {
        token=strtok(stbuf,DELIMITER);
        groupindex=atoi(token);
        token=strtok(NULL,DELIMITER);
        nb=atoi(token);
        if(!nb){ continue;} //This line prevents run-time error "Allocating zero bytes"
        aofgroups[groupindex].neighbours=nb;
        aofgroups[groupindex].nbarray=(unsigned int *)Malloc(nb*(sizeof(unsigned int)));
        for(nbindex=0; nbindex<nb; nbindex++){
            token=strtok(NULL,DELIMITER);
            aofgroups[groupindex].nbarray[nbindex]=atoi(token);
        }
    }
    fclose(fnb);
    return groupscnt;
}

size_t binrealsearch(REAL array[], unsigned int size, REAL value) {
    unsigned int low, high, medium;
    low=0;
    high=size+1;
    while(low<high) {
        medium=(high+low)/2;
        if(array[medium]<value) {
            low=medium+1;
        } else { 
            high=medium;
        }
    }
    return low;
}


int cmp_ptr(const void *a, const void *b) {
    const REAL **left  = (const REAL **)a;
    const REAL **right = (const REAL **)b;
    return (**left > **right) - (**right > **left);
}

size_t * order_REAL(const REAL *a, size_t n) {
    const REAL **pointers = malloc(n * sizeof(const REAL *));
    if(pointers==NULL) { perror("malloc2: "); }
    size_t i;

    for (i = 0; i < n; i++) pointers[i] = a + i;

    qsort(pointers, n, sizeof(const REAL *), cmp_ptr);

    size_t *indices = malloc(n * sizeof(size_t));
    if(indices==NULL) { perror("malloc2: "); }

    for (i = 0; i < n; i++) indices[i] = pointers[i] - a;

    free(pointers);

    return indices;
}

void shuffle(unsigned int *index, unsigned int n) {
    unsigned int k, ind1, tmp;
    for (k = 0; k < n-1; k++) {
        ind1 = (k+1)+(lrand48() % (n-k-1));
        tmp=index[k];
        index[k]=index[ind1];
        index[ind1]=tmp;
    }
}

void findplace(people *person, places *place) {
    REAL x, y;
    unsigned int rc=0;
    unsigned int ppgrp=0;
    people *t=person;
    while(t->nextingroup){
        t=t->nextingroup;
        ppgrp++;
    }
    //if(ppgrp+place->cnt>place->capacity){
    //    return;
    //}
    x=place->posx+(drand48()*place->sidex);     //Pick a random spot in the place
    y=place->posy+(drand48()*place->sidey);
    person->posx=x;
    person->posy=y;
    t=person;
    while(t->nextingroup) {
        REAL r = DISTMIN; //r will sets the distance between the agents of the same group. It's a RN in the range [0, distance for the place kind]
        REAL ang = 2*M_PI*drand48();
        t=t->nextingroup;
        t->posx=x+r*cos(ang);
        t->posy=y+r*sin(ang);
    }
}

#define DISTANCE_THRESHOLD 0.25
#define MAX_FORCE 0.5f

unsigned int EquilibratePlace(places *thisPlace){
    unsigned int ppneq=0;   //Persons that didn't find equilibrium counter    
    people *ptg,*othrg;     //"g" stands for the pointers in the same _G_roup
    group *ptl, *othrl;     //... and "l" stands for _L_eaders as each group struct has a pointer to the leader of the group
    unsigned int cnt;
    REAL dist, velMag;
    REAL dsrx, dsry;
    REAL steerx, steery, accMag;
    int t;
    int T_eq=100;
    float maxF;
    unsigned int cntgip=0;
    for(ptl=thisPlace->firstgroup; ptl!=NULL; ptl=ptl->nextgroup) {
      cntgip++;
    }
    if (cntgip==0) { return ppneq; }
    group *tempaog[cntgip];
    cntgip=0;
    for(ptl=thisPlace->firstgroup; ptl!=NULL; ptl=ptl->nextgroup) {
      tempaog[cntgip]=ptl;
      cntgip++;
    }
    unsigned int j, k, ind;

    //FILE *gp = popen("gnuplot -persist", "w"); 
    maxF=MAX_FORCE;
    t=0;
    //fprintf(gp, "set xrange[%f:%f]\nset yrange[%f:%f]\n", thisPlace->posx, thisPlace->posx+thisPlace->sidex, thisPlace->posy, thisPlace->posy+thisPlace->sidey);  //DEBUG
    do{
        ppneq=0;
        for (k = 0; k < cntgip-1; k++) {               /* shuffle the array with the pointers to the groups */
	  ind = (k+1)+(lrand48() % (cntgip-k-1));
	  ptl=tempaog[k];
	  tempaog[k]=tempaog[ind];
	  tempaog[ind]=ptl;
	}
        for(k=0; k<cntgip; k++) {
	    ptl=tempaog[k];
            for(ptg=ptl->groupfirst; ptg!=NULL; ptg=ptg->nextingroup){
                //Behaviour #1: Separate agent pp from other (othr) agents            
                cnt=0;
                dsrx=0.;
                dsry=0.;
                for(j=k+1; j<cntgip; j++) {
		    othrl=tempaog[j];
                    if(othrl->group_id == ptl->group_id){
		       writelog(TRUE,APPLICATION_RC,"same group id! %d\n", othrl->group_id);		        
                    }
                    for(othrg=othrl->groupfirst; othrg!=NULL; othrg=othrg->nextingroup){
                        dist=DISTANCE(ptg->posx,ptg->posy,othrg->posx,othrg->posy);
                        if(dist==0){
			  writelog(FALSE,APPLICATION_RC,"ERROR: got dist=0. %d r_%d=(%lf %lf), %d r_%d=(%lf %lf)\n", ptl->group_id, ptg->id, ptg->posx, ptg->posy, othrl->group_id, othrg->id, othrg->posx, othrg->posy);
                          writelog(TRUE,APPLICATION_RC,"Place: %f %f %f %f\n", thisPlace->posx, thisPlace->posy, thisPlace->posx+thisPlace->sidex, thisPlace->posy+thisPlace->sidey);
                        }
                        if(dist<thisPlace->distance){
                            cnt++;
                            dsrx=dsrx+(ptg->posx-othrg->posx)/(dist*dist);
                            dsry=dsry+(ptg->posy-othrg->posy)/(dist*dist);
                        }
                    }
                }
                if(cnt>0){
                    dsrx=dsrx/cnt;
                    dsry=dsry/cnt;
                    ppneq++;
                    steerx=dsrx-ptg->velx;
                    steery=dsry-ptg->vely;
                    ptg->accx=ptg->accx+steerx;       //Update acceleration
                    ptg->accy=ptg->accy+steery;
                }

                //Behaviour #2: if agent pp is not a leader of the group, get close its leader
                if(!ptg->groupleader){
                    dsrx=ptg->group->groupfirst->posx-ptg->posx;
                    dsry=ptg->group->groupfirst->posy-ptg->posy;
                    if(dsrx*dsrx+dsry*dsry>DISTANCE_THRESHOLD*thisPlace->distance*thisPlace->distance){ //if the position of the leader is greater then the half of the social distance
                        steerx=dsrx-ptg->velx;
                        steery=dsry-ptg->vely;
                        ptg->accx+=steerx;
                        ptg->accy+=steery;
                    }
                }

                //Update velocity and position via 1st Euler
                accMag = sqrt(ptg->accx*ptg->accx + ptg->accy*ptg->accy);
                if(accMag!=0. && accMag>maxF){
                    ptg->accx=ptg->accx/accMag*maxF;
                    ptg->accy=ptg->accy/accMag*maxF;
                }
                ptg->velx=ptg->velx+ptg->accx; /* t=1 so this is "correct" */
                ptg->vely=ptg->vely+ptg->accy;
                ptg->posx=ptg->posx+ptg->velx;
                ptg->posy=ptg->posy+ptg->vely;
                while(ptg->posx < thisPlace->posx){
                    ptg->posx = 2*thisPlace->posx - ptg->posx;
                }
                while(ptg->posx > thisPlace->posx + thisPlace->sidex){
                    ptg->posx = 2*(thisPlace->posx + thisPlace->sidex) - ptg->posx;
                }
                while(ptg->posy < thisPlace->posy){
                    ptg->posy = 2*thisPlace->posy - ptg->posy;
                }
                while(ptg->posy > thisPlace->posy + thisPlace->sidey){
                    ptg->posy = 2*(thisPlace->posy + thisPlace->sidey) - ptg->posy;
                }
                ptg->velx=0.0;  //If no force acts on the agent then the agent stops its motion
                ptg->vely=0.0;
                ptg->accx=0.0;   //Reset force
                ptg->accy=0.0; 
            }
            ptl->move=1; 
        }
        t++;
        //fprintf(gp, "set offset 1,1,1,1\n");
        //fprintf(gp, "plot \'-\' u 1:2:\(sprintf\(\"\%d\"\), \$3\) with labels point pt 7 offset char 1,1 title \'t=%d\'\n",t);
        //for(ptl=thisPlace->firstgroup; ptl!=NULL; ptl=ptl->nextgroup){
        //        for(ptg=ptl->groupfirst; ptg!=NULL; ptg=ptg->nextingroup){
        //            fprintf(gp, "%lf %lf %d\n",ptg->posx, ptg->posy, ptg->group->group_id);
        //    }
        //}
        //fprintf(gp, "e\n");
        //fprintf(gp, "pause(0.1)\n");
    } while(t<T_eq && ppneq);
    //fclose(gp);
    return ppneq;
}

void couplegroupplace(unsigned int whichplace, group *whichgroup, places *aofplaces, unsigned int peopleingroup) { //This function updates the data
    people *ptping = whichgroup->groupfirst;        //Pointer To People In the Group
    unsigned int cntingroup;  
    findplace(ptping,&aofplaces[whichplace]);
    cntingroup=0;
    whichgroup->done=whichplace;
    do {
        ptping=ptping->nextingroup;
        cntingroup++;
    } while(ptping!=NULL);
    if(peopleingroup!=cntingroup) {
        writelog(FALSE,APPLICATION_RC,"inconsistent number of people in group %d/%d\ngroup_id: %d\n",peopleingroup,cntingroup,whichgroup->group_id); 
    }
    if(aofplaces[whichplace].firstgroup==NULL) {
        aofplaces[whichplace].firstgroup=whichgroup;
    } else {
        aofplaces[whichplace].lastgroup->nextgroup=whichgroup;
    }
    aofplaces[whichplace].lastgroup=whichgroup;
    //if(aofplaces[whichplace].last==NULL) {
    //    aofplaces[whichplace].lastleader=ptlp;
    //} else {
    //    (aofplaces[whichplace].last)->next=&(aofpeople[whichpeople]);
    //    aofplaces[whichplace].last=ptlp;      
    //}
    aofplaces[whichplace].lastgroup->nextgroup=NULL;
    aofplaces[whichplace].cnt+=cntingroup;
}      

main(int argc, char *argv[]) {

    char ConfFileName[MAXFILELENGTH]="";
    char Directory[MAXFILELENGTH]="";  
    char peoplefilename[MAXSTRLEN]="";
    char neighboursfilename[MAXFILELENGTH]="";
    char *po; /* used for options' parsing */
    FILE *pdf, *gnuplot_data;
    unsigned int nchildren, nyoung, nadult, nelder, totalnumberofpeople=0, totalnumberofgroups=0;
    float socialDist=0.0;
    unsigned int nodp, *aodp;
    unsigned int endtime, starttime, currenttimeslice;
    long int seed;
    REAL xdim, ydim;
    REAL xcenter, ycenter;
    REAL area, perimeter;
    /* REAL maxnopref; */
    int **tp;
    int dummycap, dummyrig, dummyatt;
    REAL dummydist, dummyarea;
    char scratch[MAXSTRLEN];
    REAL *probability;
    unsigned int numberofprob=2;
    int i, j, k;
    char *agestrings[]={"children","young people","adult people","elder people"};
    writelog(FALSE, APPLICATION_RC,"Beginning ppr\n");
    FILE *gp;
    REAL __tStart=Wtime();
    REAL __tEnd;

    REAL choice;

    if(argc<MINARG) {
        Usage(argv[0]);
    }
    
    for(i = 1; i < argc; i++) {
        po = argv[i];
        if (*po++ == '-') {
            /*
               'L'
               'c'
               's'
               'n'
               'g'
               'h'
               'v'
               'p' 
             */

            switch (*po++) {
                case 'L':
                    SKIPBLANK
                        strncpy(LogFileName,po,MAXFILELENGTH);
                    break;
                case 'c':
                    SKIPBLANK
                        strncpy(ConfFileName,po,MAXFILELENGTH);
                    break;
                case 's':
                    SKIPBLANK
                        strncpy(Directory,po,MAXFILELENGTH);
                    break;
                case 'n':
                    SKIPBLANK
                        totalnumberofpeople = strtoul(po,0,10);
                    break;
                case 'f':
                    SKIPBLANK
                        strncpy(neighboursfilename,po,MAXFILELENGTH);
                    break;
                case 'g':
                    SKIPBLANK
                        strncpy(peoplefilename,po,MAXFILELENGTH);
                    break;
                case 'v':
                    verbose=TRUE;
                    break;
                case 'h':
                    Usage(argv[0]);
                    exit(OK);
                    break;
                case 'p':
                    plot_progress=TRUE;
                    gp = popen("gnuplot -persist", "w"); 
                    break;
                case 'd':
                    SKIPBLANK
                        socialDist = strtof(po,NULL);
                    break;
                default:
                    writelog(FALSE,APPLICATION_RC,"Unknown option: %s", argv[i]);
                    Usage(argv[0]);
                    exit(APPLICATION_RC);
                    break;
            }
        }
    }
    
    dictionary *ini;
    writelog(FALSE,OK,"ReadParameters: open data file <%s>\n",ConfFileName);
    char key[MAXSTRLEN];
    if(strlen(Directory)>0) {
        if(chdir(Directory)<0) { writelog(TRUE,APPLICATION_RC,"Error changing directory:%s\n",Directory); }
    }
    if(strlen(ConfFileName)==0) { writelog(TRUE,APPLICATION_RC,"No Config file"); }
    ini = iniparser_load(ConfFileName);
    READINTFI(seed, "random seed");
    srand48(seed);
    READINTFI(nodp, "Number of different kinds of places");
    aodp=(unsigned int *)Malloc(nodp*(sizeof *aodp));
    unsigned int totalnumberofplaces=0;
    for(i=1; i<=nodp; i++) {
        snprintf(scratch,sizeof(scratch),"Number of places of kind %d",i);
        READINTFI(aodp[i-1],scratch);
        totalnumberofplaces+=aodp[i-1];
    }
    places *aofplaces=(places *)Malloc(sizeof(places)*totalnumberofplaces);
    READREALFI(xcenter,"X coordinate of the center");
    READREALFI(ycenter,"Y coordinate of the center");
    READREALFI(area,"area of the place");
    READREALFI(perimeter,"perimeter of the place");
    //READREALFI(timeslices,"Total number of time slices");
    xdim=(perimeter*0.5+sqrt((perimeter*0.5)*(perimeter*0.5)-4*area))/2;
    //ydim=perimeter*0.5-xdim; DEBUG
    ydim=2*xdim;
    xdim=2*xdim;
    writelog(FALSE,APPLICATION_RC,"xdim=%f,ydim=%f\n",xdim,ydim);
    probability=(REAL *)Malloc(numberofprob*sizeof(REAL));
    REAL scratchprob;
    READREALFI(scratchprob,"Probability of picking a preferred place");
    probability[0] = scratchprob;
    if(probability[0]>1. || probability[0]<0.){
        writelog(TRUE,APPLICATION_RC,"Invalid probability of picking a preferred place: %lf\n",probability[0]);
    }
    probability[1] = 1-probability[0];
    writelog(FALSE,APPLICATION_RC,"Preference probability (1)=%lf\n", probability[0]);
    writelog(FALSE,APPLICATION_RC,"Preference probability (2)=%lf\n", probability[1]);
    writelog(FALSE,APPLICATION_RC,"Total number of places %d\n",totalnumberofplaces);
    unsigned int placescnt=0;
    for(i=1; i<=nodp; i++) {  
        snprintf(scratch,sizeof(scratch),"Rigidity of places of kind %d",i);
        READINTFI(dummyrig,scratch);
    	if(socialDist==0.0) {
            snprintf(scratch,sizeof(scratch),"Distance in places of kind %d",i);
            READREALFI(dummydist,scratch);
    	} else {
	    dummydist = socialDist;
	}
        for(k=0; k<aodp[i-1]; k++) {
            aofplaces[placescnt].kind=i;
            aofplaces[placescnt].firstgroup=NULL;
            aofplaces[placescnt].lastgroup=NULL;
            aofplaces[placescnt].rigid=dummyrig;
            aofplaces[placescnt].distance=dummydist;
            snprintf(scratch,sizeof(scratch),"Area of place %d of kind %d",k+1, i);
            READINTFI(dummyarea,scratch);
            aofplaces[placescnt].area=dummyarea;
            aofplaces[placescnt].capacity=(int)(dummyarea/(dummydist*dummydist));
            aofplaces[placescnt].sidex=sqrt(dummyarea);
            aofplaces[placescnt].sidey=aofplaces[placescnt].sidex;
            aofplaces[placescnt].cnt=0;
            snprintf(scratch,sizeof(scratch),"X Position of place %d of kind %d",k+1, i);
            READREALFI(aofplaces[placescnt].posx,scratch); /* =xdim*drand48(); */
            if(abs(aofplaces[placescnt].posx-xcenter)>xdim*0.5) {
                REAL dist=abs(aofplaces[placescnt].posx-xcenter);
                writelog(FALSE,APPLICATION_RC,"Attention: xcoord of place %d of kind %d looks strange %f %f (center is %f, size %f)\n",k+1,i,aofplaces[placescnt].posx,dist,xcenter,xdim);
            }
            snprintf(scratch,sizeof(scratch),"Y Position of place %d of kind %d",k+1, i);      
            READREALFI(aofplaces[placescnt].posy,scratch); /* =ydim*drand48(); */
            if(abs(aofplaces[placescnt].posy-ycenter)>ydim*0.5) {
                REAL dist=abs(aofplaces[placescnt].posy-ycenter);	
                writelog(FALSE,APPLICATION_RC,"Attention: ycoord of place %d of kind %d looks strange %f %f (center is %f, size %f)\n",k+1,i,aofplaces[placescnt].posy,dist,ycenter,ydim);
            }
            placescnt++;
        }
    }
    unsigned int nop[NUMBEROFAGES];
    if(strlen(peoplefilename)==0) { READSTRFI(peoplefilename,"Name of the file with people groups");}
    if(totalnumberofpeople==0) {
        totalnumberofpeople=ReadNumberofPeopleFromFile(peoplefilename);  
    }
    if(totalnumberofgroups==0) {
        totalnumberofgroups=ReadNumberofLeadersFromFile(peoplefilename);  
    }
    
    printf("DEBUG: total number of people:%d. total number of groups:%d\n",totalnumberofpeople,totalnumberofgroups); 
    
    people *aofpeople=(people *)Malloc(sizeof(people)*totalnumberofpeople);
    group *aofgroups=(group *)Malloc(sizeof(people)*totalnumberofgroups);
    tp=(int **)makematr(NUMBEROFAGES,nodp,sizeof(int));  
    for(i=0; i<NUMBEROFAGES; i++) {
        for(j=1; j<=nodp; j++){
            snprintf(scratch,sizeof(scratch),"Attractivity of places of kind %d for %s",j,agestrings[i]);
            READINTFI(tp[i][j-1],scratch);
            if (tp[i][j-1]<1 || tp[i][j-1]>numberofprob){
                writelog(TRUE,APPLICATION_RC,"Invalid values of attractivity for kind %d: %d\n",j,tp[i][j-1]);
            }
        }
    }
    iniparser_freedict(ini);    
    unsigned int *placeavaile=(unsigned int *)Malloc(sizeof(unsigned int)*totalnumberofplaces);
    unsigned int howmanypeopleavail=ReadGroupsData(peoplefilename,neighboursfilename,aofpeople,aofgroups,totalnumberofpeople,tp, nodp, xcenter, ycenter, xdim, ydim, &starttime, &endtime);
    unsigned int *groupsavaile=(unsigned int *)Malloc(sizeof(unsigned int)*totalnumberofgroups);
    unsigned int cntingroup;

    REAL *distances=(REAL *)Malloc(sizeof(REAL)*totalnumberofplaces);
    unsigned int howmanygroupsavail;
    unsigned int howmanyplacesavail;
    unsigned int whichplace;
    unsigned int whichgroup;
    unsigned int preference;  
    unsigned int indexgroup=0;
    unsigned int index, tries;
    for(currenttimeslice=starttime; currenttimeslice<endtime; currenttimeslice++){
        howmanyplacesavail=0;
        howmanygroupsavail=0;
        for(i=0; i<totalnumberofgroups; i++){
            if(aofgroups[i].time_in==currenttimeslice){
                groupsavaile[howmanygroupsavail]=i;
                howmanygroupsavail++;
            }
            if(aofgroups[i].done<0 && aofgroups[i].time_in > currenttimeslice && aofgroups[i].time_out < currenttimeslice){
                aofgroups[i].move=1;
                groupsavaile[howmanygroupsavail]=i;
                howmanygroupsavail++;
            }
            if(aofgroups[i].done>=0 && aofgroups[i].time_in > currenttimeslice && aofgroups[i].time_out < currenttimeslice){
                if(drand48()<ONEHALF){
                    aofgroups[i].done=-(aofgroups[i].done+2);   //In this way we record the last place visited by the agent: if last place=0-->done=-2, last place=1-->done=-3, and so on...
                    aofgroups[i].move=1;
                    groupsavaile[howmanygroupsavail]=i;
                    howmanygroupsavail++;
                }
            }
        }
        if(howmanygroupsavail){
            shuffle(groupsavaile, howmanygroupsavail);
        }        
        for(i=0; i<totalnumberofplaces; i++){
            group *gpt, *lgpt=NULL;
            people *ppt;
            aofplaces[i].cnt=0;
            for(gpt=aofplaces[i].firstgroup; gpt!=NULL; gpt=gpt->nextgroup){
                if(gpt->time_out == currenttimeslice){
                    gpt->done=-1;
                    gpt->move=0;
                    continue;
                }
                //If I'll stay in the park for the current timeslice
                if(gpt->move && drand48()<ONEHALF){ //The group throws a coin and decides to move toward a friend (if available) or not
                    for(j=0; j<gpt->neighbours; j++){
                        unsigned int friend_index=gpt->nbarray[j];
                        if(aofgroups[friend_index].done==i){
                            if(drand48()<ONEHALF){
                                //printf("DEBUG: t=%d. friends found. IDs: %d, %d place: %d, done: %d, %d\n",currenttimeslice, gpt->group_id, friend_index, i, 
                                //gpt->done, aofgroups[friend_index].done);
                                gpt->move=0;
                                aofgroups[friend_index].move=0;
                                ppt=gpt->groupfirst;
                                REAL ang=drand48()*M_PI;
                                ppt->posx=aofgroups[friend_index].groupfirst->posx+aofplaces[i].distance*cos(ang);
                                ppt->posy=aofgroups[friend_index].groupfirst->posy+aofplaces[i].distance*sin(ang);
                                for(ppt=ppt->nextingroup; ppt!=NULL; ppt=ppt->nextingroup){
                                    ang=drand48()*M_PI;
                                    ppt->posx=gpt->groupfirst->posx+DISTMIN*cos(ang);
                                    ppt->posy=gpt->groupfirst->posy+DISTMIN*sin(ang);
                                }
                                break;
                            }        
                        }
                    }
                }
                if(gpt->move && drand48()<ONEHALF){ //The group throws a coin and decide to wander in the place or not
                    findplace(gpt->groupfirst,&aofplaces[i]);
                }

                aofplaces[i].cnt+=gpt->groupcnt;
                if(!lgpt){
                    aofplaces[i].firstgroup=gpt;
                } else {
                    lgpt->nextgroup=gpt;
                }
                aofplaces[i].lastgroup=gpt;
                lgpt=gpt;
            }
            if(!lgpt){
                aofplaces[i].firstgroup=NULL;
                aofplaces[i].lastgroup=NULL;
            } else {
                aofplaces[i].lastgroup->nextgroup=NULL;
            }
            if(aofplaces[i].cnt<aofplaces[i].capacity){
                placeavaile[howmanyplacesavail]=i;
                howmanyplacesavail++;
            }
        }
        indexgroup=0;
        while(howmanygroupsavail && howmanyplacesavail) {
            shuffle(placeavaile,howmanyplacesavail);
            whichgroup=groupsavaile[indexgroup];                      //Pick a person from the available persons and make sure it's a leader
            if(aofgroups[whichgroup].groupfirst->groupleader!=1) {
                writelog(TRUE,APPLICATION_RC,"Unexpected people in group %d\n",whichgroup);
            }
            choice=drand48();
            for(i=0; i<numberofprob; i++) {
                if(probability[i]>choice) break; //Pick a preference 
            }
            preference=i+1;
            howmanygroupsavail--;
            for(i=0; i<howmanyplacesavail; i++) {
                distances[placeavaile[i]]=DISTANCE(aofgroups[whichgroup].groupfirst->posx,aofgroups[whichgroup].groupfirst->posy,aofplaces[placeavaile[i]].posx,aofplaces[placeavaile[i]].posy);
            }
            tries=0;
            do {
                if(tries>howmanyplacesavail) { break; } /* too many attempts */
                index=howmanyplacesavail*drand48();
                whichplace=placeavaile[index];
                tries++;
            } while(aofplaces[whichplace].cnt+1>aofplaces[whichplace].capacity && -aofgroups[whichgroup].done+2!=whichplace); //MOD 
            if(tries<=howmanyplacesavail) {
                for(i=0; i<howmanyplacesavail; i++) {
                    if( -(aofgroups[whichgroup].done+2) == placeavaile[i] ) { continue;}
                    if( (aofplaces[placeavaile[i]].cnt<aofplaces[placeavaile[i]].capacity && aofplaces[placeavaile[i]].rigid) &&
                            ((aofgroups[whichgroup].groupfirst->preference[aofplaces[placeavaile[i]].kind-1]==preference && aofgroups[whichgroup].groupfirst->preference[aofplaces[whichplace].kind-1]!=preference) ||      
                             ((aofgroups[whichgroup].groupfirst->preference[aofplaces[placeavaile[i]].kind-1] == aofgroups[whichgroup].groupfirst->preference[aofplaces[whichplace].kind-1]) &&
                              (distances[placeavaile[i]] < distances[whichplace]) ))) {
                        whichplace=placeavaile[i];
                        index=i;
                    }
                }

                couplegroupplace(whichplace, &aofgroups[whichgroup], aofplaces, aofgroups[whichgroup].groupcnt);
                if(aofplaces[whichplace].cnt>=aofplaces[whichplace].capacity) {
                    howmanyplacesavail--;
                    for(i=index; i<howmanyplacesavail; i++) {placeavaile[i]=placeavaile[i+1];}      
                }
            }
            indexgroup++;
        }
        unsigned int peoplecoupled=0;
        unsigned int peopleuncoupled=0;
        int ppneqtot=0;
        int ppneq;
        printf("----TIME SLICE: %d----\n", currenttimeslice);
	if(plot_progress) gnuplot_data = fopen ("temp.dat","w");
	for(i=0; i<totalnumberofplaces; i++) {
            ppneq=EquilibratePlace(&aofplaces[i]);
            ppneqtot+=ppneq;
            printf("Place: %d kind: %d coords: (%f,%f) cnt: %d capacity: %d area: %lf side: %lf ppneq=%d people:", i, aofplaces[i].kind, aofplaces[i].posx-xcenter, aofplaces[i].posy-ycenter, aofplaces[i].cnt, aofplaces[i].capacity, aofplaces[i].area, aofplaces[i].sidex, ppneq);
            group *l;
            people *g;
            for(l=aofplaces[i].firstgroup; l!=NULL; l=l->nextgroup){
                for(g=l->groupfirst; g!=NULL; g=g->nextingroup){
                    printf(" %d %d %d (%7.1f %7.1f)", g->group->group_id, g->id, g->age, g->posx-xcenter, g->posy-ycenter);
                    peoplecoupled++;
                }
            }
            printf("\n");
            if((plot_progress)){ //&&(48<currenttimeslice)&&(currenttimeslice<64)){  // && peoplecoupled>=100  && currenttimeslice%10==0
		// fprintf(gp, "set title(\"TIME SLICE: %d\")\nplot '-' with dot\n", currenttimeslice);
        	// printf("set title(\"TIME SLICE: %d\")\nplot '-' with dot\n", currenttimeslice);
                for(l=aofplaces[i].firstgroup; l!=NULL; l=l->nextgroup){
                    for(g=l->groupfirst; g!=NULL; g=g->nextingroup){
                        fprintf(gnuplot_data, "%7.1f %7.1f\n", g->posx-xcenter, g->posy-ycenter);
                        // printf("%7.1f %7.1f\n", g->posx-xcenter, g->posy-ycenter);
                    }
                }
        	// fprintf(gp, "e\n");
        	// printf("e\n");
            }
        }
	if(plot_progress) fclose(gnuplot_data);
	char temp_data_fname[MAXFILELENGTH] = "temp.dat";
	// char temp_data_fname[MAXFILELENGTH] = "temp.dat";
	if(plot_progress) fprintf(gp, "set title(\"TIME SLICE: %d\")\nplot '%s' with dots\n", currenttimeslice, temp_data_fname);
        // if(plot_progress) fprintf(gp, "e\n");
        printf("List of uncoupled groups\n");
        for (i=0; i<totalnumberofgroups; i++) {
            // printf("%d done: %d\n", i, aofpeople[i].done);
            if(aofgroups[i].done<0 && aofgroups[i].time_in>=currenttimeslice && aofgroups[i].time_out<currenttimeslice) {
 	        people *t=aofgroups[i].groupfirst;
                peopleuncoupled+=aofgroups[i].groupcnt;
                do {
                    printf(" %d %d %d (%7.1f %7.1f)", aofgroups[i].group_id, t->id, t->age, t->posx-xcenter, t->posy-ycenter);
                    t=t->nextingroup;
                } while(t!=NULL);
            }
        }
        printf("\n");
        printf("total people coupled=%d\n",peoplecoupled);  
        printf("total people uncoupled=%d\n",peopleuncoupled);
        printf("total number of groups=%d\n",totalnumberofgroups);    
        printf("total number of people=%d\n",totalnumberofpeople);
        printf("ppneq_total=%d\n",ppneqtot);
    }
    for(i=0; i<totalnumberofpeople; i++) {
        free(aofpeople[i].preference);
    }

    free(aodp);
    free(aofplaces);
    free(probability);
    free(aofpeople);
    free(placeavaile);
    free(groupsavaile);
    free(distances);
    freematr(NUMBEROFAGES,(void **)tp);
    __tEnd=Wtime();
    writelog(FALSE, APPLICATION_RC,"PPR in %g seconds\n",__tEnd-__tStart);
    if(plot_progress) fclose(gp);
}
