import os

types = ['park', 'school', 'office']
places_per_type = {'park':['playground', 'sport', 'bench', 'amenities', 'field', 'walkway']}

def main():

    header = ['name', 'type', 'lon', 'lat', 'surface', 'perimeter']

    dirs = [d for d in next(os.walk('data'))[1] if len(d.split('_')) ==3 and d.split('_')[1] in types]
    #dirs = [d for d in next(os.walk('data'))[1] if d.split('_')[0] in types]

    data = {t:{} for t in types}
    for k in dirs:
        
        print(k)

        if os.path.exists('data/{}/PPR_real.ini'.format(k)):
            d = {}
            with open('data/{}/PPR_real.ini'.format(k)) as f:
                for line in f:
                    try:
                        h,v = line.split('=')
                        d[h] = v.strip()
                    except:
                        pass
            lon       = d['X coordinate of the center']
            lat       = d['Y coordinate of the center']
            area      = d['area of the place']
            perimeter = d['perimeter of the place']
            kinds     = int(d['Number of different kinds of places'])
            kinds_dict = []
            for i in range(1,kinds+1):
                dd = {}
                dd['number'] = d['Number of places of kind {}'.format(i)]
                dd['capacity'] = d['Max capacity of places of kind {}'.format(i)] # [d['Capacity of place {} of kind {}'.format(j,i)] for j in range(1,int(dd['number'])+1)]
                kinds_dict.append(dd)
            site_type = k.split('_')[1]
            #site_type = k.split('_')[0]
            data[site_type][k] = {}
            data[site_type][k]['name'] = k
            data[site_type][k]['type'] = site_type
            data[site_type][k]['lon']  = lon
            data[site_type][k]['lat']  = lat
            data[site_type][k]['surface'] = area
            data[site_type][k]['perimeter'] = perimeter
            for h,dd in zip(places_per_type[site_type],kinds_dict):
                data[site_type][k]['{} number'.format(h)] = dd['number']
                data[site_type][k]['{} capacity'.format(h)] = dd['capacity']

    for t,tdata in data.items():
        if tdata:
            with open('data/sites_{}.csv'.format(t), 'w') as f:
                t_header = header + [x for h in places_per_type[t] for x in ['{} number'.format(h), '{} capacity'.format(h)]]
                print(', '.join(t_header), file=f)
                for v in tdata.values():
                    print(', '.join(str(v[h]) for h in t_header), file=f)


if __name__ == "__main__":
    main()
