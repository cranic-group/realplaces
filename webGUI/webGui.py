import tornado.ioloop
import tornado.web
import os
import asyncio
import sys
import configparser
import subprocess

from tornado.options import define, options, parse_command_line

define("port", default=8888, help="run on the given port", type=int)
define("debug", default=True, help="run in debug mode")


TIME_SLOTS = [10,15,20,30,60]
DAYS = { 'Monday':'Lunedì', 'Tuesday':'Martedì', 'Wednesday':'Mercoledì', 'Thursday':'Giovedì', 'Friday':'Venerdì', 'Saturday':'Sabato', 'Sunday':'Domenica'}
DEFAULT_GROUP_SIZE = 5
DEFAULT_PEAK = 1000
DEFAULT_DIST = 0.0
CONFIG_FILE = 'config.ini'

class MainHandler(tornado.web.RequestHandler):
     
    def get_parameters(self):
        error_msg = ""
        error = False
        group_size = self.get_argument("group_size")
        try:
            group_size = int(group_size)
            if group_size == 0:
                raise ValueError
        except ValueError:
            error = True
            group_size = 0
            error_msg += "La dimensione del gruppo deve essere un NUMERO intero diverso da 0. "
        peak = self.get_argument("peak")
        try:
            peak = int(peak)
            if peak == 0:
                raise ValueError
        except ValueError:
            error = True
            peak = 0
            error_msg += "Il picco d'ingressi deve essere un NUMERO intero diverso da 0. "
        dist = self.get_argument("dist")
        try:
            dist = float(dist)
            if dist == 0.0:
                raise ValueError
        except ValueError:
            error = True
            dist = 0.0
            error_msg += "La distanza sociale deve essere un NUMERO maggiore di 0. "
        min_visit_time = self.get_argument("minVT")
        try:
            min_visit_time = int(min_visit_time)
            if min_visit_time == 0:
                raise ValueError
        except ValueError:
            error = True
            min_visit_time = 0
            error_msg += "Il tempo minimo di visita deve essere un NUMERO intero diverso da 0. "
        mean_visit_time = self.get_argument("meanVT")
        try:
            mean_visit_time = int(mean_visit_time)
            if mean_visit_time == 0:
                raise ValueError
        except ValueError:
            error = True
            min_visit_time = 0
            error_msg += "Il tempo medio di visita deve essere un NUMERO intero diverso da 0. "
        time_slot = self.get_argument("time_slot")
        day = self.get_argument("day")
        
        return time_slot, day, group_size, peak, dist, mean_visit_time, min_visit_time, error, error_msg
    
    def get(self):
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
        self.render("home.html", error="", timeSlots=TIME_SLOTS, days=DAYS, groupSize=DEFAULT_GROUP_SIZE, peakSize=DEFAULT_PEAK, socialDist=DEFAULT_DIST,
                    minVT=config['SITES']['min_visit_time'] , meanVT=config['SITES']['mean_visit_time'] )

    async def post(self):
        time_slot, day, group_size, peak, dist, mean_visit_time, min_visit_time, error, error_msg = self.get_parameters()

        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
        config['SITES']['min_visit_time'] = str(min_visit_time)
        config['SITES']['mean_visit_time'] = str(mean_visit_time)
        with open(CONFIG_FILE, 'w') as configfile:
            config.write(configfile)
        
        if error :
            self.render("home.html", error=error_msg, timeSlots=TIME_SLOTS, days=DAYS, groupSize=group_size, peakSize=peak, socialDist=dist,
                    minVT=config['SITES']['min_visit_time'], meanVT=config['SITES']['mean_visit_time'] )
        else:
            cmd = f"python3 ../in_site_epidemic.py -c {CONFIG_FILE} -g data/graphs/firenze/EP05/sb_graph_new.pickle -t {time_slot} -s {group_size} -n 1 -e 1 --city firenze --cutoff 2 --dist {dist} --day {day} --peak {peak} -i .. -o static && python3 ../analysis/plot_results.py"
            await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE)
            self.render("simulation.html", time_slot=time_slot, day=day, group_size=group_size, peak=peak, dist=dist, minVT=min_visit_time, meanVT=mean_visit_time )


class ResultsHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("results.html")        
    
    
class ProcHandler(tornado.web.RequestHandler):
    def get(self):
        path = os.path.join(os.path.dirname(__file__), "static")
        #fname = f"{path}/completed"
        fname = f"{path}/readyPlots"
        if os.path.isfile(fname):
            os.remove(fname)
            self.set_status(200)
        else:
            self.set_status(404)      


def main():
    parse_command_line()
    app = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/results", ResultsHandler),
            (r"/proc", ProcHandler),
        ],
        #cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        #xsrf_cookies=True,
        debug=options.debug,
    )
    app.listen(options.port)
    print(f'Listening on http://localhost:{options.port}')
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
    
