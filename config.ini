[RANDOM]
seed=None

[CITY]
city = 'vo'

[TERRITORY]
# origin: coordinates of the bottom left corner of the grid: (lat, lon); CRS: WGS84

origin = {'firenze':(43.7258716, 11.1497598), 'sabaudia':(41.2430859, 12.9152005), 'viterbo':(42.2981485, 11.9045499), 'vo':(45.3000357, 11.6116795), 'trento':(45.9775306, 11.0224735), 'rieti':(42.3090846, 12.7111355)}

ntiles = {'firenze':(15, 12), 'sabaudia':(16, 20), 'viterbo':(26, 32), 'vo':(6, 6), 'trento':(13, 19), 'rieti':(23,22)}

# Size in meters of each tile in the grid: (x length, y length)
tile   = (1000, 1000)

territory_file = {k:'./data/population/{}/territory_projected.pickle'.format(k) for k in ['firenze','sabaudia','viterbo','vo','trento','rieti']}

[PROJECTION]
input_CRS='EPSG:4326'
output_CRS='EPSG:32632'

[POPULATION]
# the size of the sample population. size=0 means no sampling, i.e., consider the total population of the city
# this is the size of the cities used in the paper: Florence:363060, Viterbo:66598, Sabaudia:21274
size = 0

# how to determine the age-stratification, can be "real" (i.e., get age frequencies from the csv population file), "uniform" or a dict {group_name:group_frequency}
groups  = 'real'

group_names = ['children', 'young', 'adults', 'elderly']

age_groups = [18, 35, 65, 100] 
# Function used to compute the fitness of the nodes; supported values are 'one' (constant f), 'uniform' (uniform in (0,1)), 'lognormal', 'pareto' 
fitness_func = 'lognormal'

# Whether to generate households or not
households = True

# Generator used to create population
# if mode == (generator, random_gen_type) the population will be generated using a pseudo random generator, where random_gen_type can be either 'uniform' or 'normal'
# if mode == reader    the population will be read from a csv file
mode = ('generator','uniform')

popfile = {k:'./data/population/{}/population_0.csv'.format(k) for k in ['firenze','sabaudia','viterbo','vo','trento','rieti']}

[DISTANCES]
# Distance between nodes in the same tile
zero_dist=500
# Function used to compute distances between nodes in different tiles
# currently supported:  
# dist_func = 'euclidean'  # the Euclidean distance is used
# dist_func = 'one'    # the distance between any two nodes is set to 1
# dist_func = ('euclideanPower', beta)  # the Euclidean distance to power beta is used
dist_func = ('euclideanPower', 2)

[SOCIALITY]
avg_deg = 10
# Whether to use a data-driven or a constant age-mixing matrix S
# if group2group_type != 'constant' the below group2group matrix is read and used
group2group_type = 'real' 
group2group = {'children':{'children':10.3139591, 'young':1.4095328, 'adults':3.004145, 'elderly':0.3194044}, 'young':{'children':1.1164900, 'young':5.0501872, 'adults':2.671315, 'elderly':0.5556512}, 'adults':{'children':1.4129868, 'young':1.5862171, 'adults':3.777106, 'elderly':0.8300762}, 'elderly':{'children':0.2498358, 'young':0.5487018, 'adults':1.380431, 'elderly':1.2556003}}


#group2group = {'children':{'children':.25, 'young':.25, 'adults':.25, 'elderly':.25},
#               'young':{'children':.25, 'young':.25, 'adults':.25, 'elderly':.25},
#               'adults':{'children':.25, 'young':.25, 'adults':.25, 'elderly':.25},
#              'elderly':{'children':.25, 'young':.25, 'adults':.25, 'elderly':.25}}

group2site  = {'children':{'park':{'morning':.05, 'afternoon':.25, 'evening':.0}, 'school':{'morning':.8, 'afternoon':.8, 'evening':.0}, 'office':{'morning':.0, 'afternoon':.0, 'evening':.0}},
               'young':{'park':{'morning':.05, 'afternoon':.25, 'evening':.0}, 'school':{'morning':.8, 'afternoon':.8, 'evening':.0}, 'office':{'morning':.0, 'afternoon':.0, 'evening':.0}},
               'adults':{'park':{'morning':.025, 'afternoon':.125, 'evening':.05}, 'school':{'morning':.0, 'afternoon':.0, 'evening':.0}, 'office':{'morning':.8, 'afternoon':.8, 'evening':.0}},
               'elderly':{'park':{'morning':.2, 'afternoon':.05, 'evening':.0}, 'school':{'morning':.0, 'afternoon':.0, 'evening':.0}, 'office':{'morning':.3, 'afternoon':.3, 'evening':.0}}}

in_site_params = {'graph_distance_cutoff':2, 'euclidean_distance_cutoff':2, 'noise_volume':.05}

interaction_params = {'interaction_distance_p':[.2,.5], 'avg_interactions':[1,2], 'geo_distance_max':[1000,None]} 
#interaction_params = {'interaction_distance_p':[.2,.5,.8], 'avg_interactions':[1,2,5]} 

[SITES]
sites_csv = 'data/sites_park.csv'
appeal_weights = {'park':{'surface':1, 'perimeter':1, 'playground':1, 'sport':1, 'bench':1, 'amenities':1, 'field':1, 'walkway':1}}
		# 'school':{'surface':1, 'playgrounds':1, 'cycle_lanes':1, 'arenas':1, 'food_drink':1, 'sports':1},
                # 'office':{'surface':1, 'playgrounds':1, 'cycle_lanes':1, 'arenas':1, 'food_drink':1, 'sports':1}}
appeal_calc = {'surface':lambda x:x/2500, 'perimeter':lambda x:x/500, None:lambda x,y:x*min(100,y)/2}
# in appeal_calc, x is the number of places of that type, y is the capacity of each place. for appeal_calc we set the maximum capacity to 100
# capacity_calc = {'morning':lambda surf:surf/2500, 'playgrounds':lambda x:x, 'cycle_lanes':lambda x:x, 'arenas':lambda x:x, 'food_drink':lambda x:x, 'sports':lambda x:x}
popularity_times = 'pop_dict.pickle'
min_visit_time  = 30
mean_visit_time = 60
# max_visit_time  = inf

[PLOT_OPTIONS]
group_colors = {'children':'cyan', 'young':'magenta', 'adults':'brown',
                'elderly':'grey'}
site_colors = {'park':'forestgreen', 'school':'pink', 'office':'tan'}

[EPIDEMICS]
# number or fraction of initially exposed nodes
pi    = 100
delta = .001

# edge infection rate
beta = {'SA':.1, 'SC':.1, 'household':.1, 'default':.5, 'park':{'playground':.1, 'sport':.1, 'bench':.1, 'amenities':.1, 'field':.1, 'walkway':.1, 'default':.1}, 'school':{'default':.1}}

# average incubation length
sigma = 7

# minimum length of disease
tau = 7

# recovery probabilty -- nb: this is a daily rate!
gamma = {'children':.5, 'young':.3, 'adults':.15, 'elderly':.05}

# death probability -- nb: this is a daily rate!
omega = {'children':.00001, 'young':.0001, 'adults':.001, 'elderly':.01}

