import os
import shutil
import itertools
from collections import Counter
import pickle
import sys
from sys import getsizeof
import argparse
import configparser as cp
import time
from timeit import default_timer as timer
import pandas as pd
import numpy as np
import subprocess
from scipy.spatial.distance import pdist, squareform, euclidean
from scipy.sparse import csr_matrix
from sklearn.metrics import pairwise_distances_chunked as pdist_chunks
# from sklearn.neighbors import NearestNeighbors
# from sklearn.cluster import DBSCAN
from numpy.random import default_rng
from copy import deepcopy
import matplotlib.pyplot as plt
import seaborn as sns

import igraph as ig

from create_social_base import get_input_data_structures, get_distance_array, preprocess_distances, get_grid_distances, get_grid_centers, compute_grid_distances
from lib.libfunc import read_from_config, project_coord
from create_sites import places_per_type

from network_epidemics import build_si_model, read_epidemics_params



#####################################################
# placeholders!
def leave_site(pop_type, site, time_spent=None):
    if time_spent<site['min_visit_time']:
        return False
    else:
        leave_prob = site['leaving_prob']
        return np.random.random()<leave_prob

def go_to_site(pop_type, site_type=None, hour=None):
    # this measures the ratio (to be normalized) of people of the given type entering the given site at the given hour
    return 1


def get_interest(pop_type, site_type=None, hour=None):
     
    type_range = range(pop_type.min(),pop_type.max()+1)
    # this must be replaced with a call to a function that gives the probability per group per 
    group_interest = np.array([go_to_site(g, site_type, hour)*(pop_type==g).sum() for g in type_range])
    group_interest = group_interest/group_interest.sum()

    return group_interest[pop_type]

#####################################################

def normalize_distances(distance_array, pop_type_array):
    # Compute and normalize distances
    type_range = range(pop_type_array.min(),pop_type_array.max()+1)
    for t in type_range:
        is_t = pop_type_array==t
        distance_array[is_t] = distance_array[is_t]/distance_array[is_t].sum()
    return distance_array


def compute_site_probability(site, distance_array, pop_type_array, hour):
    """ Computes the array of probability  of all the edges of the node n with all the other nodes
        in the population. 

        P(n, m) = ...

        Returns:
            numpy array which stores in each element the probability of edge n, j where n is the input node
            and j are all the other nodes. P(n,n) is set to 0.
    """
    # get normalized interest scores 
    interest = get_interest(pop_type_array, site['type'], hour)
    probability = distance_array * interest
    return probability


def site_schedule(g_sb, site, site_params, data, max_group_size=3, group_members_filename='group_members.csv', group_neighbors_filename='group_neighbors.csv', out_path='.'):
    
    dist_info = data['dist_info']
    territory = data['territory']
    pop_info  = data['pop_info']
    pop_type_array = np.asarray(g_sb.vs['type'])
    pop_tile_id_array = np.asarray(g_sb.vs['tile_id'])
    N = len(pop_type_array)
    
    ########################################
    # Compute the coordinates of the grid centers
    grid_centers = get_grid_centers(territory)
    # Compute tile_0 distance array: grid_distance_array_0
    grid_distance_array_0 = compute_grid_distances(0, grid_centers, dist_info)
    # preprocess grid distances
    preprocessed_grid_distance_array_0 = preprocess_distances(grid_distance_array_0, dist_info)
    distance_array = 1/get_site_to_pop_distance_array(site['tile_id'], grid_distance_array_0, territory, pop_tile_id_array)

    # array that tracks who went to the site at any time
    in_site = np.zeros(N, dtype=bool)
    # dict that tracks who arrived at time t and is currently in the site
    in_site_groups = {}
    # dict that tracks who left at time t
    left_site_groups = {}
    # lists that track the number of people arriving and leaving at each time slot
    num_arrivals = []
    num_left     = []
    num_present  = [0]
    a = 0
    
    for t,v in sorted(site['pop_per_slot'].items()):
        print(f'site_schedule: at time {time.strftime("%H:%M", time.gmtime(t[0]*60))}-{time.strftime("%H:%M", time.gmtime(t[1]*60))}')
        # print(f'expected inside: {v}')
        # print(f'already inside: {num_present[-1]}')

        # choose who leaves and stays
        staying = 0
        temp = {}
        left = 0
        for t0,groups in in_site_groups.items():
            to_keep = []
            for group in groups:
                if leave_site(group[0][1], site, t[0]-t0[0]):
                    left_site_groups.setdefault((t0[0],t[0]),[]).append(group)
                    left += len(group)
                else:
                    to_keep.append(group)
                    staying += len(group)
            if to_keep:
                temp[t0] = to_keep
        num_left.append(left)
        in_site_groups = temp
        
        # print(f'staying: {staying}')
        # print(f'left: {left}')
        
        # select only those that are not already in the site
        temp_distance_array = distance_array*(1-in_site)
        # normalize with respect to the group but only considering those not in the site
        temp_distance_array = normalize_distances(temp_distance_array, pop_type_array)
        # compute the probability for each of those not in the site
        probs = compute_site_probability(site, temp_distance_array, pop_type_array, t[0])

        # rescale these probabilities taking in consideration the average group size and that people that have already beeb in the park cannot arrive
        s = probs[in_site==0].sum()
        if s>0:
            probs[in_site==0] *= 1/s
        probs[in_site==1] = 0
        
        probs = probs*(max_group_size/sum(range(max_group_size+1)))*max(0,(v-staying))
        
        # print(f'sum of probs: {probs.sum()}')
        # type_range = range(pop_type_array.min(),pop_type_array.max()+1)
        # for t_ in type_range:
        #     is_t = pop_type_array==t_
        #     print(probs[is_t].sum())

        # select the people arriving at the park
        arriving = np.where(probs>np.random.random(probs.shape))[0].tolist()

        # get groups for each arriving person
        new_groups = get_site_groups(g_sb, arriving, in_site, max_group_size)
        # update in_site array
        num_arrivals.append(sum(len(group) for group in new_groups))
        # print(f'arriving: {num_arrivals[-1]}')
        if new_groups:
            in_site[np.asarray([u[0] for group in new_groups for u in group])] = 1
        num_present.append(staying+num_arrivals[-1])

        in_site_groups[t] = new_groups
    
    for t0,groups in in_site_groups.items():
        left_site_groups[(t0[0],t[1])] = groups
    sorted_intervals = sorted(left_site_groups.keys())
    intervals_order = {k[0]:i for i,k in enumerate(sorted(site['pop_per_slot'].keys()))}
    intervals_order[t[1]] = len(site['pop_per_slot'])
    groups = [[u[0] for u in group] for k in sorted_intervals for group in left_site_groups[k]]

    tot_in_site = in_site.sum()
    in_site_per_group = [in_site[pop_type_array==i].sum() for i in range(4)]
    print(f'site_schedule: total number of people in the site: {in_site.sum()}')
    print(f'site_schedule: % in the site per type:\n  children {in_site_per_group[0]/tot_in_site*100}\n  young {in_site_per_group[1]/tot_in_site*100}\n  adults {in_site_per_group[2]/tot_in_site*100}\n  elderly {in_site_per_group[3]/tot_in_site*100}')
    sns.set_context('talk')
    sns.set_style("white")
    plt.figure(figsize=(6,3))
    plot_df = {'time slot':list(range(len(num_arrivals)))*3, 'number of agents':num_present[1:]+num_arrivals+list(site['pop_per_slot'].values()), 'hue':['present']*len(num_present[1:])+['arrivals']*len(num_arrivals)+['predicted']*len(list(site['pop_per_slot'].values()))}
    sns.lineplot(data=plot_df, x='time slot', y='number of agents', hue='hue', style='hue')
    plt.legend(loc='upper left')
    # plt.xlabel('time slot')
    # plt.ylabel('number of agents')
    plt.savefig(os.path.join(out_path,'population_per_hour.png'), bbox_inches='tight')
    plt.cla()

    time_in = [t_out-t_in for (t_in,t_out),grps in sorted(left_site_groups.items()) for _g in grps]
    print(f'average visit time: {np.mean(time_in)}')
    time_hist = sorted(Counter(time_in).items())
    plt.plot(*(list(zip(*time_hist))), marker='x', label='data')
    first_slot = list(site['pop_per_slot'].keys())[0]
    time_slot_length = first_slot[1]-first_slot[0]
    geometric_times = (site['min_visit_time']-time_slot_length)+time_slot_length*np.random.geometric(site['leaving_prob'],len(time_in)) 
    print(f'average of related geometric distribution: {np.mean(geometric_times)}')
    plt.plot(*list(zip(*sorted(Counter(geometric_times).items()))), marker='+', label='geometric')
    plt.legend()
    plt.savefig(os.path.join(out_path,'visit_time_distribution.png'))
    plt.cla()

    # valutare se il calcolo dei neighborhood si possa rendere più efficiente considerando solo il sottografo delle persone al parco (non credo se il cutoff>1)
    graph_distance_cutoff = site_params['graph_distance_cutoff']
    print(f'site_schedule: graph distance cutoff set to {graph_distance_cutoff}')
    neighborhoods = get_neighborhoods(g_sb, graph_distance_cutoff)          
    if neighborhoods is None:
        print('site_schedule: [ERROR] neighborhoods cannot be computed, exit')
        sys.exit()
    else:
        print('site_schedule: neighborhoods computed successfully')
        print(f'site_schedule: neighborhoods shape: {neighborhoods.shape}')

    order = []
    idx = [0]
    for g in groups:
        order.extend(g)
        idx.append(idx[-1]+len(g))
    idx.pop() 
    if len(order)!=len(set(order)):
        print({k:v for k,v in Counter(order).items() if v>1})
    reachable = np.asarray(neighborhoods[order][:,order].todense())
    print(f'site_schedule: reachable shape: {reachable.shape}, reachable dtype: {reachable.dtype}')
    # reachable = reachable.cumsum(0).cumsum(1)
    # cummax of bools is memory efficient: 
    neighs = np.maximum.reduceat(np.maximum.reduceat(reachable,idx,axis=0),idx,axis=1)
    np.fill_diagonal(neighs,False)
    print('site_schedule: reachable groups done')
    print('neighs done!')

    # print(sorted_intervals)
    # print(intervals_order)

    with open(group_members_filename, 'w') as f:
        print('group_id, time_in, time_out, group_size, '+', '.join([f'member{i}, age{i}' for i in range(max_group_size)]), file=f)
        gid = 0
        for t_in,t_out in sorted_intervals:
            t_in_id = intervals_order[t_in]
            t_out_id = intervals_order[t_out]
            groups = left_site_groups[(t_in,t_out)]
            for group in groups:
                group_size = len(group)
                group += [('','')]*(max_group_size-group_size)
                print(f'{gid}, {t_in_id}, {t_out_id}, {group_size}, '+', '.join([f'{memb[0]}, {memb[1]}' for memb in group]), file=f)
                gid += 1
    max_neighbors = neighs.sum(axis=1).max()
    # argmax = neighs.sum(axis=1).argmax()
    # print(f'group {argmax} has {max_neighbors} neighs')
    with open(group_neighbors_filename, 'w') as f:
        print('group_id, group_nb_size, '+', '.join([f'group_nb_{i}' for i in range(max_neighbors)]), file=f)
        for gid in range(len(neighs)):
            nb = np.where(neighs[gid])[0].tolist()
            nb_size = len(nb)
            nb = nb+['']*(max_neighbors-nb_size)
            print(f'{gid}, {nb_size}, '+', '.join([f'{i}' for i in nb]), file=f)
   

def create_site_graphs(g_sb, site, site_params, data, max_group_size=3, social_distance=0.0, bash_command="site_dynamics/ppr_real", dump_filename='output_ppr.txt', in_path='.', out_path='.'):
   
    # noise_volume              = site_params['noise_volume']
    euclidean_distance_cutoff = site_params['euclidean_distance_cutoff']
    
    base_path = os.path.join(in_path,'data')
    site_path = f"{base_path}/{site['name']}"
    if not os.path.exists(site_path):
        os.makedirs(site_path)
    while True:
        run_dir  = f'run_{np.random.randint(10000):04}'
        run_path = os.path.join(site_path,run_dir)
        if not os.path.exists(run_path):
            os.makedirs(run_path)
            break
    
    group_members_filename = 'group_members.csv'
    group_neighbors_filename = 'group_neighbors.csv'
    group_members_path   = os.path.join(run_path,group_members_filename)
    group_neighbors_path = os.path.join(run_path,group_neighbors_filename)

    # create site schedule and dump to file
    site_schedule(g_sb, site, site_params, data, max_group_size=max_group_size, group_members_filename=group_members_path, group_neighbors_filename=group_neighbors_path, out_path=out_path)
   
    group_members_path   = os.path.join(run_dir,group_members_filename)
    group_neighbors_path = os.path.join(run_dir,group_neighbors_filename)
    ########### RUN PPR ############
    command = os.path.join(in_path, bash_command) + ' -c PPR_real.ini'
    command += f' -s {site_path}'
    # command += f' -n {ntot}'
    command += f' -g {group_members_path}'
    command += f' -f {group_neighbors_path}'
    if social_distance:
        command += f' -d {social_distance}'
    command += ' -p'
    while True:
        temp_filename = dump_filename.rsplit('.',1)[0]+'_{}.txt'.format(np.random.randint(100000))
        if not os.path.exists(temp_filename):
            dump_filename = temp_filename
            break
    print(f'create_site_graphs: now running {command}')
    # sys.exit()
    with open(dump_filename, 'w') as fout:
        subprocess.run(command, stdout=fout, shell=True)
    time_places = get_places(dump_filename)
    
    # remove input and output files of ppr
    #### DEBUG ####
    # os.remove(dump_filename)
    # shutil.rmtree(run_path, ignore_errors=True)
    ###############

    # with open('time_places.pickle', 'wb') as f:
    #     pickle.dump(time_places,f)

    ########## BUILD GRAPHS ###########
    graphs = []
    num_places = len(time_places[0])
    time = len(time_places)
    for t in range(time):
        G = ig.Graph()
        names = []
        groups = []
        coords = []
        layer = []
        # weight = []
        for place in time_places[t]:
            idxs  = place['people']['index']
            coord = place['people']['coord']
            gidxs = place['people']['group']
            if idxs:
                G.add_vertices(len(idxs))
                names.extend(idxs)
                groups.extend(gidxs)
                coords.extend(coord)
                # pairwise euclidean distances, by default in condensed form
                reachable_geo = get_reachable_geo(coord, euclidean_distance_cutoff) 
                # from condensed form indices to normal indices
                idx_map = np.asarray(list(itertools.combinations(range(len(coord)), 2)))
                selected = idx_map[np.where(reachable_geo)]
                if selected.size>0:
                    edges = np.arange(G.vcount()-len(idxs),G.vcount())[selected]
                    G.add_edges(edges)
                    # layer is the place name
                    place_type_name = places_per_type[site['type']][place['place_type_id']-1]
                    layer += [place_type_name]*len(edges)
                    # weight += [get_risk(place_type_name)]*len(edges)
        # noise_edges = add_noise(G, places, volume=noise_volume)
        # G.add_edges(noise_edges)
        # layer += ['noise']*len(noise_edges)
        # weight += [get_risk('noise')]*len(noise_edges)
        G.vs['name'] = names
        G.vs['group'] = groups
        G.vs['coords'] = coords
        G.es['layer'] = layer
        # G.es['weight'] = weight
        graphs.append(G)

    #### MERGE BASED ON THE 'name' ATTRIBUTE ####
    # G = ig.union(graphs,byname=True)

    return graphs


def one_day_epidemics(daily_graphs, config_file, n_runs, candidate_infected=None, verbose=True):

    epidemic_params = read_epidemics_params(config_file)
    # estatus_list = read_from_config(config_file,'EPIDEMICS','status')
    # estatus_dict = {i+1:s for i,s in enumerate(estatus_list)}
    
    G = ig.union(daily_graphs,byname=True)
    pop_size = G.vcount()
    try:
        n_infected = epidemic_params['pi']
    except:
        n_infected = int(round(pop_size*epidemic_params['delta']))

    layers = set().union(*[set(g.es['layer']) for g in daily_graphs])
    model = build_si_model(epidemic_params, pop_size, existing_layers=layers, verbose=verbose)

    # place_names = places_per_type[site['type']]
   
    runs = []
    for i in range(n_runs):
        
        if not candidate_infected:
            candidate_infected = G.vs['name']
        else:
            candidate_infected = list(set(G.vs['name']).intersection(set(candidate_infected)))
        if len(candidate_infected)<n_infected:
            print('one_day_epidemics: [WARNING] there are not enough candidate infected in the graph, will draw infected individuals from the entire graph')
            candidate_infected = G.vs['name']
        initial_infected_set = set(np.random.choice(candidate_infected, n_infected, replace=False).tolist())
        
        iterations = []
        for site_graph in daily_graphs:
            # create and run model on each SC
            # Model initial status configuration
            model.add_graph(site_graph)
            initial_infected_nodes = [v.index for v in site_graph.vs if v['name'] in initial_infected_set]
            model.reset(infected_nodes=initial_infected_nodes)
            # config = mc.Configuration()
            # config.add_model_initial_configuration('Infected', initial_exposed_nodes)
            # model.set_initial_status(config)
            # we run 2 iterations because iteration 0 is just a set-up
            iters = model.iteration_bunch(2) #, verbose=True)
            inf_0 = [site_graph.vs[k]['name'] for k,v in iters[0]['status'].items() if v==2]
            inf_1 = [site_graph.vs[k]['name'] for k,v in iters[1]['status'].items() if v==2]
            iterations.append({'infected_0':inf_0, 'infected_1':inf_1})

        runs.append(iterations)
        if i%(n_runs/10)==0:
            print('one_day_at_the_park: at iter', i)

    return runs


########### SITES ##########

def get_site_to_pop_distance_array(site_tile_id, grid_distance_array_0, territory, pop_tile_id_array):
    """ Compute the distance between one population node and all the others.
        Assumes that the distance between two nodes equals the distance between
        the centers of their respective tiles.
        Uses the function get_grid_distances to compute distances between tile centers.
    """
    # Compute distance among grid centers
    tile_distances = get_distance_array(site_tile_id, territory, grid_distance_array_0)
    # Assign distances to all the points
    distance_array = tile_distances[pop_tile_id_array]
    return distance_array


def build_sites(in_path, config_file, sites_type=None, city=''):
    
    #sites = read_sites(config_file, read_only=sites_type)
    sites = read_sites(in_path, config_file, read_only=sites_type, city=city)
    appeal_weights, appeal_calc = read_appeal(config_file)
    for s in sites:
        s['appeal'] = get_appeal(s, appeal_weights, appeal_calc) 
    pop_dict_fname  = read_from_config(config_file, 'SITES', 'popularity_times')
    min_visit_time  = read_from_config(config_file, 'SITES', 'min_visit_time')
    mean_visit_time = read_from_config(config_file, 'SITES', 'mean_visit_time')
    for s in sites:
        with open(os.path.join(in_path,'data',s["name"],pop_dict_fname), 'rb') as f:
            s['pop_dict'] = pickle.load(f)
        s['min_visit_time'] = min_visit_time
        s['mean_visit_time'] = mean_visit_time
    # print(sites)
    return sites  


def read_sites(in_path, config_file, read_only=None, city=''):
    '''read sites info from config file and csv file
    read_only can be used to specify a single type of sites of interest
    returns sites info as a list of dicts [{key-->value},...,{key-->value}] and sites coords as a numpy array of shape (M,2) where M is the number of sites'''
    
    # print("reading sites info from config file: {}".format(config_file))
    config = cp.ConfigParser()
    config.read(config_file)
    ####
    sites_csv   = os.path.join(in_path,eval(config.get('SITES','sites_csv')))
    df = pd.read_csv(sites_csv, skipinitialspace=True)
    if read_only:
        if isinstance(read_only,str):
            read_only = [read_only]
        try:
            df = df[df['type'].isin(read_only)]
        except:
            print('[read_sites] warning: read_only parameter "{}" is invalid, will be ignored'.format(read_only))
            pass
    sites_coords = df[['lon','lat']].to_numpy().tolist()
    # rough check of whether coords must be projected
    if sites_coords[0][0]<100:
        inCRS  = read_from_config(config_file, 'PROJECTION', 'input_CRS')
        outCRS = read_from_config(config_file, 'PROJECTION', 'output_CRS')
        sites_coords = np.asarray([project_coord((c[0], c[1]), inCRS, outCRS) for c in sites_coords])
    sites_lons, sites_lats = list(zip(*sites_coords))

    data = get_input_data_structures(in_path, config_file, city)
    territory = data['territory']
    tx, ty   = territory['tile']
    ntx, nty = territory['ntiles']
    x0, y0   = territory['origin']
    xtile  = (np.asarray(sites_lons) - x0)//tx
    ytile  = (np.asarray(sites_lats) - y0)//ty
    sites_tile_ids = xtile + ntx*ytile
    sites_tile_ids = sites_tile_ids.astype(int)
    df['tile_id'] = sites_tile_ids

    sites = df[[c for c in df.columns if not c in ('lon', 'lat')]].to_dict('records')
  
    return sites


def read_appeal(config_file):
    '''read from config file how to calculate appeal scores for each type of site'''
    config = cp.ConfigParser()
    config.read(config_file)
    ####
    appeal_weights = eval(config.get('SITES', 'appeal_weights'))
    appeal_calc    = eval(config.get('SITES', 'appeal_calc'))
    # groups = eval(config.get('POPULATION', 'groups'))
    # g2g_probabilities = np.array([[g2g_probabilities[k][h] for h in groups.keys()] for k in groups.keys()])
    return appeal_weights, appeal_calc


def get_appeal(site, appeal_weights, appeal_calc):
    '''compute the appeal of a site based on its attractions'''
    t = site['type']
    appeal_weights = appeal_weights[t]
    # print([appeal_calc[attraction](site[attraction]) for attraction,w in appeal_weights.items()])
    score = 0
    for attraction,w in appeal_weights.items():
        if attraction in appeal_calc:
            try:
                score += appeal_calc[attraction](site[attraction+' number'],site[attraction+' capacity'])*w
            except:
                score += appeal_calc[attraction](site[attraction])*w
        else:
            try:
                score += appeal_calc[None](site[attraction+' number'],site[attraction+' capacity'])*w
            except:
                try:
                    score += appeal_calc[None](site[attraction])*w
                except:
                    pass
        # print(attraction, score)
    return score


#### SOCIAL CONTEXTS ####


def line_parser(line):
    d = {}
    fields = line.strip().split(': ')[1:]
    d['place_id']      = int(fields[0].split()[0])
    d['place_type_id'] = int(fields[1].split()[0])
    d['place_xcoord'], d['place_ycoord'] = eval(fields[2].split()[0])
    d['people_count']  = int(fields[3].split()[0])
    d['place_capacity']  = int(fields[4].split()[0])
    d['place_area']  = float(fields[5].split()[0])
    d['place_side']  = float(fields[6].split()[0])
    d['people'] = {'group':[],'index':[],'age':[],'coord':[]}
    if d['people_count'] > 0:
        people = [p.split() for p in fields[7].split(')')[:-1]]
        for p in people:
            d['people']['group'].append(int(p[0])) #
            d['people']['index'].append(int(p[1])) #
            d['people']['age'].append(int(p[2])) #
            if len(p)==6:
                d['people']['coord'].append((float(p[4]),float(p[5])))
            else:
                d['people']['coord'].append((float(p[3].strip('(')),float(p[4])))
    return d


def get_places(filename):
    places = {}
    i = 0
    with open(filename) as f:
        for line in f:
            if line.startswith('----TIME SLICE:'):
                try:
                    places[i] = time_slice
                    i += 1
                except:
                    pass
                time_slice = []
            elif line.startswith('Place'):
                time_slice.append(line_parser(line))
    places[i] = time_slice
    return places


def get_site_groups(g_sb, goingto, in_site, max_group_size=3, minors=[0]):
    N = g_sb.vcount()
    in_site_set = set(np.where(in_site==1)[0].tolist()).union(set(goingto))
    bringing = np.random.randint(0,max_group_size,len(goingto)).tolist()
    site_groups = []
    for p,nfriends in zip(goingto,bringing):
        all_friends = [f for f in g_sb.vs[p].neighbors() if f.index not in in_site_set]
        if g_sb.vs[p]['type'] in minors:
            adults = [f for f in all_friends if f['type'] not in minors]
            if adults:
                adult = np.random.choice(adults)
                friends = [adult]
                nfriends -= 1
                all_friends.remove(adult)
            else:
                print(f'get_site_groups: [WARNING] individual {p} is of type {g_sb.vs[p]["type"]} but no available adult neighbor was found; it will be ignored')
                continue
        else:
            friends = [] 
        if nfriends>0:
            if nfriends<len(all_friends):
                friends.extend(np.random.choice(all_friends,nfriends,replace=False).tolist())
            else:
                friends.extend(all_friends)
        friends = [(p,g_sb.vs[p]['type'])] + [(u.index,u['type']) for u in friends]
        site_groups.append(friends)
        in_site_set = in_site_set.union(set([u[0] for u in friends]))
    return site_groups


def get_risk(place_type):
    '''returns a risk weight to be associated to all edges of the site graphs which depends upon the given place_type
    place_type may be an integer or 'noise' '''
    # this is currently a placeholder
    return 1


def add_noise(G, places, volume=None, config_file=None):
    '''adds noise edges linking (at random) nodes in different places of the same site'''
    if volume is None:
        try:
            in_site_params = read_from_config(config_file, 'SOCIALITY', 'in_site_params')
            volume = in_site_params['noise_volume']
        except:
            print('add_noise: error! noise volume not given nor retrievable')
            return
    existing_edges = G.ecount()
    to_add = int(existing_edges*volume)
    p = np.asarray([place['people_count'] for place in places])
    tot_nodes = p.sum()
    p = p/tot_nodes 
    edges = set()
    for _ in range(to_add):
        while True:
            source_place,target_place = np.random.choice(len(places),2,replace=False,p=p)
            source = np.random.choice(places[source_place]['people']['index'])
            target = np.random.choice(places[target_place]['people']['index'])
            if (source, target) not in edges and (target,source) not in edges:
                edges.add((source,target))
                break
    return edges


def get_igraph_adjacency(g):
    if g.is_directed():
        data = np.ones(g.ecount())
    else:
        data = np.ones(2*g.ecount())
    indptr = [0]
    indices = []
    for adjl in g.get_adjlist():
        indices.extend(adjl)
        indptr.append(len(indices))
    A = csr_matrix((data,indices,indptr), shape=(g.vcount(),g.vcount()))
    return A


def get_neighbors(g_sb,max_dist=2,min_dist=1):
    A = get_igraph_adjacency(g_sb)
    neighborhoods = sum(A**i for i in range(min_dist,max_dist+1))
    neighborhoods.setdiag(0.0)
    return neighborhoods>0


def get_neighborhoods(g_sb, cutoff=None, config_file=None):                
    if cutoff is None:
        try:
            in_site_params = read_from_config(config_file, 'SOCIALITY', 'in_site_params')
            cutoff = in_site_params['graph_distance_cutoff']
        except:
            print('get_neighborhoods: error! cutoff not given nor retrievable')
            return
    neighborhoods = get_neighbors(g_sb,cutoff)
    return neighborhoods
                

def get_reachable_geo(coords, cutoff=None, config_file=None):
    if cutoff is None:
        try:
            in_site_params = read_from_config(config_file, 'SOCIALITY', 'in_site_params')
            cutoff = in_site_params['euclidean_distance_cutoff']
        except:
            print('get_reachable_geo: error! cutoff not given nor retrievable')
            return
    distances = pdist(coords) # this is by default in condensed form
    return distances<=cutoff



##### MAIN #####


def main():

    parser=argparse.ArgumentParser(description="")
    parser.add_argument('-c', '--config', dest='config', type=str, default=DEFAULT_CONFIG_FILE, help='configuration file in .ini format')
    parser.add_argument('-i', '--input_path', dest='in_path', type=str, default=DEFAULT_IN_PATH, help='input path, used to retrieve all files and directory used by the simulator')
    parser.add_argument('-o', '--output_path', dest='out_path', type=str, default=DEFAULT_OUT_PATH, help='input path, used to retrieve all files and directory used by the simulator')
    parser.add_argument('-g', '--graph', dest='graph', type=str, help='the graph to be used for the epidemic simulation')
    parser.add_argument('-t', '--time_slot_length', dest='time_slot_length', type=int, default=60, help='length in minutes of each time slot; must be a divisor or a multiple of 60')
    parser.add_argument('-s', '--group_size', dest='group_size', type=int, default=3, help='maximum number of individuals in a group')
    parser.add_argument('-n', '--epidemic_runs', dest='epidemic_runs', type=int, default=1, help='number of runs of the one-day epidemic in the site')
    parser.add_argument('-e', '--experiments', dest='n_exps', type=int, default=1, help='number of independent experiments')
    parser.add_argument('--city', dest='city', type=str, default='', help='specify the city')
    parser.add_argument('--cutoff', dest='cutoff', type=int, default=0, help='specify the cutoff for graph neighborhood')
    parser.add_argument('--dist', dest='dist', type=float, default=0.0, help='specify the imposed social distancing')
    # parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='whether to print information about the simulation')
    parser.add_argument('--day', dest='day', type=str, default='Sunday', help='specify the day of the week')
    parser.add_argument('--peak', dest='peak', type=int, default=None, help='specify the weekly peak popularity; if None, the site\'s appeal is used')
    parser.add_argument('--infected_tiles', dest='infected_tiles', nargs="+", default=None, help='specify the tiles from which infected people must be drawn')
    

    args = parser.parse_args()
    in_path          = args.in_path
    out_path         = args.out_path
    config_file      = args.config
    city             = args.city
    cutoff           = args.cutoff
    social_distance  = args.dist
    graph_file       = args.graph
    time_slot_length = args.time_slot_length
    group_size       = args.group_size
    n_runs           = args.epidemic_runs
    n_exps           = args.n_exps
    day              = args.day
    peak             = args.peak
    infected_tiles   = args.infected_tiles
    if infected_tiles:
        infected_tiles   = [int(t) for t in args.infected_tiles]

    config_file = os.path.join(in_path, config_file)
    graph_file = os.path.join(in_path, graph_file)
    data        = get_input_data_structures(in_path, config_file, city)
    sites       = build_sites(in_path, config_file, sites_type=None, city=city)
    site_params = read_from_config(config_file, 'SOCIALITY', 'in_site_params')
    if cutoff:
        site_params['graph_distance_cutoff'] = cutoff
    g_sb = ig.read(graph_file)
    
    site = sites[0]
    print('creating site graphs for the following site:')
    for k,v in site.items():
        print(f'{k}: {v}')

    if not peak:
        peak = site['appeal']
    if (time_slot_length<60 and not 60%time_slot_length==0) or (time_slot_length>60 and not time_slot_length%60==0):
            print('[ERROR] the length of a time slot is not a divisor nor a multiple of 60 minutes')
            sys.exit()
    n_slots_per_hour = 60/time_slot_length
    n_slots_per_day  = int(24*n_slots_per_hour)
    
    if n_slots_per_hour>=1:
        site['pop_per_slot'] = {(i*time_slot_length,(i+1)*time_slot_length):round(peak*site['pop_dict'][day][int(i//n_slots_per_hour)]) for i in range(0,n_slots_per_day)}
    else:
        hours_in_slot = int(1/n_slots_per_hour)
        site['pop_per_slot'] = {(i*time_slot_length,(i+1)*time_slot_length):round(peak*np.mean(site['pop_dict'][day][i*hours_in_slot:(i+1)*hours_in_slot])) for i in range(0,n_slots_per_day)}

    mean_n_slots = (site['mean_visit_time']-site['min_visit_time'])/time_slot_length
    leave_prob   = min(1,1/(mean_n_slots+1))
    print(f'the probability to leave at the end of each time slot is {leave_prob}') 

    # if float(site['max_visit_time'])<float('inf'):
    #     max_n_slots = (site['max_visit_time']-site['min_visit_time'])/time_slot_length
    
    site['leaving_prob'] = leave_prob
    
    # for k,v in site['pop_per_hour'].items():
    #     print(f'{k}: {v}')
    
    # for j in site['pop_dict'][day]:
    #     print('#'*round(peak*j))

    out_base = out_path
    for exp_num in range(n_exps):
        if n_exps>1:
            out_path = os.path.join(out_base,str(exp_num))
        if not os.path.exists(out_path):
            os.makedirs(out_path)
        # site_schedule(g_sb, site, site_params, data, time_slots=time_slots, max_group_size=group_size, in_path=in_path)
        graphs = create_site_graphs(g_sb, site, site_params, data, max_group_size=group_size, social_distance=social_distance, in_path=in_path, out_path=out_path)
    
        with open(os.path.join(out_path,'daily_graphs.pickle'), 'wb') as f:
            pickle.dump(graphs, f)

        candidate_infected = None
        print(f'infected_tiles: {infected_tiles}')
        if infected_tiles:
            if isinstance(infected_tiles,int):
                infected_tiles = [infected_tiles]
            candidate_infected = [v.index for v in g_sb.vs if v['tile_id'] in infected_tiles]

        # print([g_sb.vs['tile_id'][i] for i in candidate_infected])
        # sys.exit()

        runs = one_day_epidemics(graphs, config_file, n_runs, candidate_infected=candidate_infected, verbose=True)

        with open(os.path.join(out_path,'one_day_epidemic_runs.pickle'), 'wb') as f:
            pickle.dump(runs, f)
            
        with open(os.path.join(out_path,'completed'), 'w') as f:
            f.write('Completed!')



if __name__ == "__main__":

    DEFAULT_IN_PATH = '.'
    DEFAULT_OUT_PATH = 'output/'
    DEFAULT_CONFIG_FILE = 'config.ini'
    main()





