#
#
#    Copyright (C) 2020 Stefano Guarino, Enrico Mastrostefano, Massimo Bernaschi, Alessandro Celestini, Marco Cianfriglia, Davide Torre and Lena Zastrow
#
#    This file is part of USN.
#
#    USN is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USN is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with USN.  If not, see <http://www.gnu.org/licenses/>.
#
#

import os
import sys
import pickle
import argparse
import configparser as cp
from timeit import default_timer as timer
import numpy as np
# from scipy.sparse import csr_matrix
from numpy.random import default_rng

import igraph as ig

from lib.libfunc import read_from_config

from ndlibA.models.CompositeModel import CompositeModel
from ndlibA.models.compartments.EdgeCategoricalAttribute import EdgeCategoricalAttribute
from ndlibA.models.compartments.NodeNumericalAttribute import NodeNumericalAttribute
from bokeh.io import output_file, save
# from celluloid import Camera

from ndlibA.viz.bokeh.DiffusionTrend import DiffusionTrend
from ndlibA.viz.bokeh.DiffusionPrevalence import DiffusionPrevalence


#### DIFFUSION MODEL PARAMETERS ####

def read_epidemics_params(config_file):
    config = cp.ConfigParser()
    config.read(config_file)
    params = {k:eval(v) for k,v in config['EPIDEMICS'].items()}
    return params


#### DEFINE FUNCTION FOR BUILDING TRENDS AND PLOTTING ####

def plot_model(model, iterations, filename_prefix='', figs=None, legend_prefix='', **line_kwargs):
    trends = model.build_trends(iterations)

    # set colors for statuses
    status2color = {
        'Susceptible':'blue',
        'Exposed':'gold',
        'Infected':'red',
        'Recovered':'lime',
        'Dead':'black',
        'Other':'gray'
    }
    diff = min(model.available_statuses.values())
    id2status = {v-diff:k for k,v in model.available_statuses.items()}
    id2color = {k:status2color[v] for k,v in id2status.items()}
    # id2color = {i:status2color[v] for i,(k,v) in enumerate(id2status.items())}

    ## PLOT DIFFUSION TREND ##
    viz = DiffusionTrend(model, trends)
    try:
        fig = figs[0]
    except:
        fig = None
    p1 = viz.plot(width=500, height=500, cols=id2color, fig=fig, legend_prefix=legend_prefix, **line_kwargs)
    if os.path.isdir(filename_prefix):
        outf = os.path.join(filename_prefix,'diffusion_trend.html')
    else:
        outf = filename_prefix+'diffusion_trend.html'
    output_file(outf)
    save(p1)

    ## PLOT DIFFUSION PREVALENCE ##
    viz2 = DiffusionPrevalence(model, trends)
    try:
        fig = figs[1]
    except:
        fig = None
    p2 = viz2.plot(width=500, height=500, cols=id2color, fig=fig, legend_prefix=legend_prefix, **line_kwargs)
    if os.path.isdir(filename_prefix):
        outf = os.path.join(filename_prefix,'diffusion_prevalence.html')
    else:
        outf = filename_prefix+'diffusion_prevalence.html'
    output_file(outf)
    save(p2)
    return p1,p2


####################################################################
#################### EPIDEMICS ON A GIVEN GRAPH ####################
####################################################################
def build_si_model(epidemic_params, pop_size, infected=None, graph=None, existing_layers=None, verbose=False):

    if graph and pop_size!=graph.vcount():
        print(f'build_sir_model: WARNING! specified pop_size is {pop_size} but the given graph has {graph.vcount()} vertices! pop_size is set to {graph.vcount()}')
        pop_size = graph.vcount()

    #############################################
    #### READ PARAMS AND DEFINE COMPARTMENTS ####
    #############################################

    #### INFECTION ####
    # infection probability for edges linking to an infected node
    try:
        beta = epidemic_params['beta']
    except:
        print('build_sir_model: could not determine the infection probability/ies ("beta") based on the given epidemic_params; will use 0.5')
        beta = .5
    if isinstance(beta, dict):
        # putting sublayers at the top level. e.g., 'park':{'bench':0.2,'playground':0.3} becomes 'park_bench':0.2, 'park_playground':0.3
        temp = {}
        for k,v in beta.items():
            if isinstance(v,dict):
                for sublayer,b in v.items():
                    temp[f'{k}_{sublayer}'] = b
            else:
                temp[k] = v
        beta = temp
        if verbose:
            print('build_sir_model: the infection probabilities are the following')
            for k,v in beta.items():
                print(f'\t{k}: {v}')
    else:
        if verbose:
            print(f'build_sir_model: the infection probability is {beta}')
        beta = {'default':beta}
    
    if graph:
        existing_layers = sorted(set(graph.es['layer']))
    elif not existing_layers:
        existing_layers = list(beta.keys())

    infection_rules = []
    for layer in existing_layers:
        if verbose:
            print(f'build_sir_model: setting infection probability for layer {layer}')
        if layer not in beta:
            b = beta.get('default')
            if b is None:
                print(f'build_sir_model: WARNING! infection probability for layer {layer} not specified and no default value is available; layer {layer} will be ignored!')
                continue
        else:
            b = beta[layer]
        if verbose:
            print(f'build_sir_model: infection probability for layer {layer} is {b}')
        infection_rules.append(EdgeCategoricalAttribute("layer", layer, probability=b, triggering_status=["Infected"]))
    
    #### INITIAL INFECTED ####
    # number of initially infected individuals
    if infected is None or isinstance(infected, int):
        if infected is None:
            try:
                try:
                    infected = epidemic_params['pi']
                except:
                    infected = int(round(pop_size*epidemic_params['delta']))
            except:
                print('build_sir_model: could not determine the number of initially infected individuals ("pi" or "delta") based on the given epidemic_params; will use 1')
                infected = 1
        initial_infected_nodes = np.random.choice(pop_size,infected,replace=False).tolist()
    else:
        initial_infected_nodes = [i for i in infected]
    if verbose:
        print('build_sir_model: the number of initially infected individuals is', len(initial_infected_nodes))
    
    ######################
    #### CREATE MODEL ####
    ######################

    model = CompositeModel(graph) 
    
    # Model statuses
    model.add_status("Susceptible")
    model.add_status("Infected")
    
    # Set inital infected
    model.reset(infected_nodes=initial_infected_nodes)
    
    # Add disease attribute, this means that no minimum disease length is imposed
    model.attributes['disease'] = np.zeros(pop_size)
    # Add infection and recovery rules
    for infection_rule in infection_rules: 
        model.add_rule("Susceptible", "Infected", infection_rule)
    
    return model


def build_sir_model(epidemic_params, pop_size, infected=None, graph=None, verbose=False):

    if graph and pop_size!=graph.vcount():
        print(f'build_sir_model: WARNING! specified pop_size is {pop_size} but the given graph has {graph.vcount()} vertices! pop_size is set to {graph.vcount()}')
        pop_size = graph.vcount()

    #############################################
    #### READ PARAMS AND DEFINE COMPARTMENTS ####
    #############################################

    #### INFECTION ####
    # infection probability for edges linking to an infected node
    try:
        beta = epidemic_params['beta']
    except:
        print('build_sir_model: could not determine the infection probability/ies ("beta") based on the given epidemic_params; will use 0.5')
        beta = .5
    if isinstance(beta, dict):
        # putting sublayers at the top level. e.g., 'park':{'bench':0.2,'playground':0.3} becomes 'park_bench':0.2, 'park_playground':0.3
        for k,v in beta.items():
            if isinstance(v,dict):
                for sublayer,b in v.items():
                    beta[f'{k}_{sublayer}'] = b
                del(beta[k])
        if verbose:
            print('build_sir_model: the infection probabilities are the following')
            for k,v in beta.items():
                print(f'\t{k}: {v}')
    else:
        if verbose:
            print(f'build_sir_model: the infection probability is {beta}')
        beta = {'default':beta}
    
    try:
        existing_layers = sorted(set(graph.es['layer']))
    except:
        existing_layers = list(beta.keys())

    infection_rules = []
    for layer in existing_layers:
        if layer not in beta:
            b = beta.get('default')
            if b is None:
                print(f'build_sir_model: WARNING! infection probability for layer {layer} not specified and no default value is available; layer {layer} will be ignored!')
                continue
            elif verbose:
                print(f'build_sir_model: infection probability for layer {layer} not specified, will use the default value {b}')
        else:
            b = beta[layer]
        infection_rules.append(EdgeCategoricalAttribute("layer", layer, probability=b, triggering_status=["Infected"]))
    
    #### RECOVERY ####
    try:
        gamma = epidemic_params['gamma']
    except:
        print('build_sir_model: could not determine the recovery probability/ies ("gamma") based on the given epidemic_params; will use 0.1')
        gamma = 0.1
    
    try:
        cure_rate = np.vectorize(gamma.__getitem__)(graph.vs['type'])
        if verbose:
           print('build_sir_model: the recovery probabilities are the following')
           for k,v in gamma.items():
               print(f'\t{k}: {v}')
    except:
        try:
            gamma = np.mean(gamma.values())
        except:
            pass
        print(f'build_sir_model: the recovery probability is set to {gamma}, regardless of the vertex type')
        cure_rate = gamma
    recovery = NodeNumericalAttribute('disease', value=0, op='==', rate=cure_rate)
    
    #### INITIAL INFECTED ####
    # number of initially infected individuals
    if infected is None or isinstance(infected, int):
        if infected is None:
            try:
                try:
                    infected = epidemic_params['pi']
                except:
                    infected = int(round(pop_size*epidemic_params['delta']))
            except:
                print('build_sir_model: could not determine the number of initially infected individuals ("pi" or "delta") based on the given epidemic_params; will use 1')
                infected = 1
        initial_infected_nodes = np.random.choice(pop_size,infected,replace=False).tolist()
    else:
        initial_infected_nodes = [i for i in infected]
    if verbose:
        print('build_sir_model: the number of initially infected individuals is', len(initial_infected_nodes))
    
    ######################
    #### CREATE MODEL ####
    ######################

    model = CompositeModel(graph) 
    
    # Model statuses
    model.add_status("Susceptible")
    model.add_status("Infected")
    model.add_status("Recovered")
    
    # Set inital infected
    model.reset(infected_nodes=initial_infected_nodes)
    
    # Add disease attribute, this means that no minimum disease length is imposed
    model.attributes['disease'] = np.zeros(pop_size)
    # Add infection and recovery rules
    for infection_rule in infection_rules: 
        model.add_rule("Susceptible", "Infected", infection_rule)
    model.add_rule("Infected", "Recovered", recovery) 
    
    return model


def run_sir(model, n_iter=None, graph=None, verbose=False):

    if graph:
        model.add_graph(graph)
        if verbose:
            print('run_sir: graph added! graph summary:')
            print(graph.summary())

    # Run the epidemics   
    if not n_iter:
        n_iter = np.inf
    iterations = model.iteration_bunch(2) #, verbose=True)
    i = 1
    currently_infected = iterations[i]['node_count'][2]
    while i<n_iter and currently_infected:
        if verbose and i%10==0:
            print(f'run_sir: the epidemics is at step {i}, the number of infected nodes is {currently_infected}')
        iters = model.iteration_bunch(1) #, verbose=True)
        it = {'iteration':i+1}
        it.update({k:v for k,v in iters[-1].items() if k!='iteration'})
        iterations.append(it)        ##
        i += 1
        currently_infected = iterations[i]['node_count'][2]
    
    return model, iterations



########### MAIN ###########
DEFAULT_OUT_PATH = 'epi_output'
DEFAULT_IN_PATH = '.'
DEFAULT_CONFIG_FILE = 'config/epidemics.ini'

def main():
    parser=argparse.ArgumentParser(description="network_epidemics: run a SIR epidemics on a given graph")
    parser.add_argument('-c', '--config', dest='config', type=str, default=DEFAULT_CONFIG_FILE, help='configuration file in .ini format')
    parser.add_argument('-o', '--output-path', dest='out_path', type=str, default=DEFAULT_OUT_PATH, help='output path, used for all output files generated as a result of the simulation')
    parser.add_argument('-i', '--input-path', dest='in_path', type=str, default=DEFAULT_IN_PATH, help='input path, used to retrieve all files and directory used by the simulator')
    parser.add_argument('-g', '--graph', dest='graph', type=str, help='the graph to be used for the epidemic simulation')
    parser.add_argument('-n', '--n-iter', dest='n_iter', type=int, default=0, help='number of steps of the epidemic simulation; if falsy, the simulation runs until the number of infected individuals becomes 0')
    parser.add_argument('-m', '--mode', dest='mode', type=str, default='random', help='the execution mode: can be "random" or the name of a vertex attribute; in the latter case, all initially infected vertices have the same (randomly chosen) value for that attribute')
    parser.add_argument('-u', '--mode-value', dest='mode_value', action='append', type=str, default=None, help='the specific value of the mode attribute, only the modes tile_id and node_id are supported')
    parser.add_argument('-r', '--n-runs', dest='n_runs', type=int, default=1, help='number of runs to execute')
    parser.add_argument('-b', '--beta', dest='beta', type=float, default=0, help='infection probability')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='whether to print information about the simulation')
    args = parser.parse_args()

    ### READ PARAMS ###
    in_path = args.in_path
    config_file = os.path.join(in_path,args.config)
    out_path = args.out_path
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    graph_file = os.path.join(in_path,args.graph)
    n_iter = args.n_iter
    mode = args.mode
    mode_value = args.mode_value
    n_runs = args.n_runs
    beta = args.beta
    verbose = args.verbose
    if verbose:
        print("\n*** Parameters ***")
        print(f"Input path: {in_path}")
        print(f"Configuration file: {config_file}")
        print(f"Output path: {out_path}")
        print(f"Input graph: {graph_file}")
        print(f"Number of iterations: {n_iter}")
        print(f"Mode: {mode}")
        print(f"Mode Value: {mode_value}")
        print(f"Number of runs: {n_runs}")
        if beta:
            print(f"Infection probability: {beta}")
        print("*** Parameters ***\n")
    
    graph = ig.read(graph_file)
    if verbose:
        print("\n*** Graph ***")
        print(graph.summary())
        print("*** Graph ***\n")
    ### clean graph layers ###
    graph.es['layer'] = ['household' if 'household' in l else l for l in graph.es['layer']]

    epidemic_params = read_epidemics_params(config_file)
    if beta:
        epidemic_params['beta'] = beta

    n_infected = None
    infected_vertices = None
    if mode in graph.vertex_attributes():
        try:
            n_infected = epidemic_params['pi']
        except:
            n_infected = int(round(graph.vcount()*epidemic_params['delta']))
        values = list(set(graph.vs[mode])) 
        np.random.shuffle(values) 
        backup_values = values.copy()
    elif mode == 'node_id':
        try:
            n_infected = epidemic_params['pi']
        except:
            n_infected = int(round(graph.vcount()*epidemic_params['delta']))
        if n_infected != len(mode_value):
            sys.exit('ERROR: configuration parameters specified in the configuration file do not match the number of --mode-value provided, see number of initial infected')
    elif mode != 'random':
        print('WARNING: Specified mode is not a vertex attribute, running in random mode')
        mode = 'random'
    mode_str = 'random_mode'

    for i in range(n_runs):
        print(f'---- At run {i} ----')
        if mode != 'random':
            while True:
                if mode == 'tile_id' and mode_value is not None:
                    #print(f'MODE VALUE: {mode_value}')
                    value = int(mode_value)
                elif mode == 'node_id':
                    if mode_value is None:
                        sys.exit("ERROR: value is missing, --mode-value is required for --mode 'node_id' ")
                    #print(f'MODE VALUE: {mode_value}')
                    value = [ int(i) for i in mode_value]
                else:
                    #value = np.random.choice(list(set(graph.vs[mode])))
                    if not values:
                        print(f'WARNING: all values for attribute {mode} have been used, already used values will be reused')
                        values = backup_values.copy()
                    value = values.pop()
                
                if mode == 'node_id':
                    selected = value
                    if verbose:
                        for idx in value:
                            print(f'Node: {idx} - Type: {graph.vs[idx]["type"]} - Tile: {graph.vs[idx]["tile_id"]} ' 
                                  f' - Household: {graph.vs[idx]["household"]} - Degree: {graph.vs[idx].degree()}')
                else:
                    selected = np.where(np.array(graph.vs[mode])==value)[0]
                    
                if len(selected)<n_infected:
                    if mode_value is not None:
                        print(f"ERROR: the selected tile has not enough vertices. Quit!")
                        sys.exit(111)
                    print(f'WARNING: not enough vertices having {mode}=={value}; selecting a different value for attribute {mode}')
                else:
                    break
            infected_vertices = np.random.choice(selected,n_infected,replace=False)
            if verbose:
                print(f'{len(infected_vertices)} vertices selected, all having attribute {mode}=={value}')
            mode_str = f'{mode}_{value}'
                
        if infected_vertices is None:
            infected_vertices = n_infected
        st = timer()
        # infected_vertices can be a LIST or an INT, in the second case the infected nodes 
        # are choosen at random by the function build_sir_model()
        model = build_sir_model(epidemic_params, graph.vcount(), infected=infected_vertices, graph=graph, verbose=verbose)
        model, iterations = run_sir(model, n_iter=n_iter, verbose=verbose)
        if verbose:
            print(f'Time to create SIR epidemic model and run {len(iterations)-1} iterations on the given graph: {timer()-st}')
        
        prefix = os.path.basename(args.graph).rsplit('.',1)[0]+f'_{mode_str}_run_{i}_'
        with open(os.path.join(out_path,prefix+'sir_iterations.pickle'), 'wb') as f:
            pickle.dump(iterations, f)
        plot_model(model, iterations, filename_prefix=os.path.join(out_path,prefix))


if __name__ == "__main__":
    main()



