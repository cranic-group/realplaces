import numpy as np
import configparser as cp

from lib.sb_lib import get_input_data_structures

def random_coord(m,M):
    x = np.random.random()
    return m+(M-m)*x

def random_surface(resize,min_surface,max_surface):
    x = np.random.lognormal()
    return min((1+resize*x)*min_surface,max_surface)

def main():

    header = 'name', 'type', 'lon', 'lat', 'surface', 'playgrounds', 'cycle_lanes', 'arenas', 'food_drink', 'sports', 'risks', 'limitations'

    types = ['park', 'school', 'office']
    numbers = [10,10,10]

    #### coordinates ####
    config_file = 'config/config_covid_sb.ini'
    data        = get_input_data_structures(config_file)
    territory   = data['territory']
    borders     = territory['borders']
    Ea,We,No,So = borders

    #### surface ####

    # expressed in square meters
    min_surface = 2500
    max_surface = 2000000
    # villa sciarra: 75000
    # parco delle cascine: 1600000
    # villa pamphili: 1800000
    resize = 10

    #### equipment ####

    prob = {}
    prob['playgrounds'] = .7
    prob['cycle_lanes'] = .5
    prob['arenas']      = .2
    prob['food_drink']  = .6
    prob['sports']      = .5

    #### risks ####
    risk = {}
    for t in types:
        risk[t] = np.random.random()

    #### limitations #####
    # we just assume 10 combinations of limitations exist and we give the integer encoding
    num_lim = 10

    data = {}
    for t,n in zip(types,numbers):
        for i in range(n):
            k = '{}_{}'.format(t,i)
            data[k] = {}
            data[k]['name'] = k
            data[k]['type'] = t
            data[k]['lon']  = random_coord(We,Ea)
            data[k]['lat']  = random_coord(So,No)
            data[k]['surface'] = random_surface(resize,min_surface,max_surface)
            for h in ['playgrounds', 'cycle_lanes', 'arenas', 'food_drink', 'sports']:
                data[k][h] = int(np.random.random()<prob[h])
            data[k]['risks'] = risk[t]
            data[k]['limitations'] = np.random.randint(0,num_lim-1)

    with open('data/test_sites.csv', 'w') as f:
        print(', '.join(header), file=f)
        for v in data.values():
            print(', '.join(str(v[h]) for h in header), file=f)


if __name__ == "__main__":
    main()
